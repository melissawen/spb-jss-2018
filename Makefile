TARGET=spb-jss-2018
TITLEPAGE=spb-jss-2018-title

all: $(TITLEPAGE).pdf $(TARGET).pdf
	evince $(TARGET).pdf &
	
SECTIONS = $(wildcard sections/*.tex)
FIGURES = $(wildcard figures/*)

$(TARGET).pdf: $(TARGET).tex $(TARGET).bib $(SECTIONS) $(FIGURES)
	pdflatex $(TARGET).tex
	bibtex $(TARGET)
	pdflatex $(TARGET).tex
	pdflatex $(TARGET).tex

$(TITLEPAGE).pdf: $(TITLEPAGE).tex
	pdflatex $(TITLEPAGE).tex	
	
edit:
	gedit Makefile *.bib *.tex &

clean:
	rm -f $(TARGET).pdf *.out *.aux *.log *.blg *.bbl *.dvi *.ps *.toc *.lot *.lof *.idx *.spl *~


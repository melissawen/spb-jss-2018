\section{Background}
\label{sec:background}

As mentioned above, this paper describes a partnership between an academic team
and a government agency with multiple, disparate goals. Accordingly, we see
this partnership not as a simple customer-supplier relationship, but as a
collaborative product development process, with ``two or more partners joining
complementary resource and experience with mutual aims, to design or develop a
new or improved product''~\citep{collaborationOverview:2012}. Such partnerships
involve additional management risks and challenges beyond those posed by
product development itself~\citep{collabfactors:1995}. Therefore, this scenario
demands special attention and leads to interesting research opportunities.

Software co-development is a kind of collaborative product
development~\citep{chesbrough2007,collaborationOverview:2012} concerning
different models of software development collaborations~\citep{kourtesis2012}.
The relationship between co-development, platforms, and ecosystems has evolved
with the advent of cloud computing~\citep{kourtesis2012}. Large-scale software
products evolved to platforms for co-development and software ecosystems, with
central coordination for software development~\citep{kourtesis2012} . According
to~\cite{kourtesis2012}, the advantages of this approach are ``decreased
software and business development costs, quicker time-to-market, improved
focus, reduced complexity, and economic profit''. They include FLOSS projects
as examples of software platforms open for all involved partners, which is the
focus of our study.

Complex and large-scale organizations, such as the public administration, have
to deal with multiple project variables. In public administration, software
products are acquired or developed in a rigid framework of contract
obligations. This factor has helped to increase ``complexities, delays or even
failed delivery of digital services''~\citep{mergel2016}. As one of the
responses for that, government agencies are adopting agile
methods~\citep{balter2011,margetts2013} ``to update large-scale legacy systems
and adapt to environmental changes and citizen requests
faster''~\citep{mergel2016}.   

A relevant aspect we discuss in this paper is the adoption of agile practices.
\cite{nerur2015challenges} recognized critical issues concerning the migration
from traditional to agile software development by comparing practices of both
methodologies. The authors point out managerial, organizational, people,
process, and technological issues to be rethought and reconfigured in an
organization for a successful migration. They concluded that agile
methodologies are more suitable in projects with a high variation of
requirements, technical capacities, and technologies. Formal and bureaucratic
organizations have more difficulty in the adoption of such methods.   

Agile methods represent a significant departure from traditional software
development and rely on a different management paradigm.
\cite{impactOfOrganizationalCulture} investigated the relationship between the
adoption of agile methodologies and organizational culture by evaluating nine
projects. They identified a set of six factors (feedback and learning,
teamwork, empowerment of people, focus on results, innovative leadership, and
mutual trust) directly linked to agile methods. They concluded that the
presence of these aspects in an organization correlates with the effective use
of agile methodologies in their projects.

In spite of the growing knowledge about agile methods, they are not always easy
to implement. \cite{melo2013agileBr} investigate the growing adoption of agile
methods in the Brazilian IT industry. The results of their survey highlight
some mismatch that companies face when developing software for public
administrations. They show that the acceptance of agile methods has changed in
the last decades. The software development industry claims to follow some of
the recommendations of the agile manifesto, but some universities and companies
are still resistant to adopt such methods.

Beyond the reasonably well-known agile methods, we also highlight here the
relevance of FLOSS values and practices. Yet, they are not so well defined.
Some studies tried to identify FLOSS practices, while others attempted to
determine the relationship between FLOSS practices and agile methods.
\cite{raymond}, in a seminal essay, described FLOSS best practices from
observations of the Linux kernel project and also reflections of his experience
with the FLOSS communities. \cite{capiluppi} examined about 400 projects to
find FLOSS project properties. In their work, they extracted generic
characterization (project size, age, license, and programming language),
analyzed the average number of people involved in the project, the community of
users, and documentation characteristics. \cite{warsta} found differences and
similarities between agile development and FLOSS practices. The authors argued
that FLOSS development might differ from agile in their philosophical and
economic perspectives; on the other hand, both approaches share the definition
of work. In its turn, \cite{fraser2006} claimed that FLOSS is a kind of Agile
software development methodology.   

Discussions about the relationship between FLOSS and Agile methods are
frequently concerned with managing the software project~\citep{gandomani2013}.
\cite{okoli2012} explained this relationship by comparing the characteristics
of FLOSS and Agile approaches. While~\cite{magdaleno2012} stated that the
relationship between FLOSS and Agile practice was still embryonic at the time
of their study (2012), others point in the same direction as our work,
reporting that FLOSS and Agile share similar
values~\citep{adams2009,tsirakidis2009,corbucci2010,gandomani2013}. For
instance, both approaches rely on self-organized teams as well as on team-wide
shared and coherent goals~\citep{adams2009, tsirakidis2009}. Several studies
also pointed out that FLOSS and Agile methods support each other in some
practices and in tracking the progress of the software development
project~\citep{guido2007,deshpande2008,adams2009,
lavazza2010,gary2011,okoli2012,rahman2018}. Moreover, \cite{during2006} and
\cite{goth2007} describe the use of FLOSS and Agile methodologies
simultaneously, which is the same approach analyzed in our research. 

Nevertheless, none of these works provide enough evidence about the
comprehensive integration of FLOSS and Agile when used simultaneously in a
project~\citep{gandomani2013}. Even recent works such as~\cite{harzl2017}
and~\cite{muller2018}, who present how FLOSS projects can apply agile
management frameworks to favor software development planning, do not advance
this issue, especially in large-scale and complex cases such as ours. 

In light of this scenario, our work presents a government-academia
collaboration for co-developing a production-level solution.  We analyzed the
decisions made during the life cycle of a real project from the FLOSS and agile
perspectives. Our findings are in line with the idea of ``continuous
activities'', which together is part of the roadmap for continuous software
engineering proposed by~\cite{fitzgerald2017}. Based on questionnaires,
interviews, and development activities data, we extracted the (continuous) best
practices that helped to harmonize the interactions between two different
development processes, as well as satisfied the management processes of
government and academia sides.

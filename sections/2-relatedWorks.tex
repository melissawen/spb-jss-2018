\section{Related Work}
\label{sec:RelatedWorks}

As mentioned above, this paper describes a partnership between an academic team and a government
agency with multiple, disparate goals. Accordingly, we see this
partnership not as a simple customer-supplier relationship, but as a
collaborative product development process, with ``two or more partners joining
complementary resource and experience with mutual aims, in order to design or
develop a new or improved product'' \citep{collaborationOverview:2012}. Such
partnerships involve additional management risks and challenges beyond those
posed by product development itself \citep{collabfactors:1995} and, therefore,
demand special attention.

\cite{collaborationOverview:2012} divide the literature on collaborative
product development (CPD) in three categories: papers that describe typical CPD
dynamics and success factors, usually case studies; papers that deal with the
process of partnership formation, such as choice of partner; and papers
discussing technological and methodological tools to improve collaboration. Our
work fits in the first and third categories, as it is a case study that
describes the progressive development and adoption of new technologies and
management techniques as the project evolved. Therefore, in this section, we
prioritize the literature dealing both with the adoption of novel management
approaches and with co-development involving either academia or government.

Software co-development is a kind of CPD
\citep{chesbrough2007,collaborationOverview:2012} concerning different models
of software development collaborations\citep{kourtesis2012}.
%
\cite{dittrich2002} discussed the co-development of services, citizenship, and
technology in the e-Government context. From four different cases, they mapped
similar issues regarding e-government development. Since e-government changes
the paradigm of several public services, the authors claim that it requires
coordination with the design and construction of supporting technical and
organizational infrastructures. Our paper reports our experience developing
such infrastructures.
%
According to \cite{kourtesis2012}, the relationship between co-development,
platforms, and ecosystems has evolved with the advent of cloud computing.  They
claim that large-scale software products evolved to platforms for
co-development and software ecosystems, with central coordination for software
development. The authors cite the advantages of this approach, such as
``decreased software and business development costs, quicker time-to-market,
improved focus, reduced complexity, and economic profit''. They include FLOSS
projects as examples of software platforms open for all involved partners,
which is the focus of our study.

Co-development is also related to knowledge transfer. \cite{wan2010} conducted
an empirical study on the influence of knowledge transfer in Software Process
Improvement (SPI). They proposed a conceptual framework composed of five
elements: ``knowledge of transfer, sources of knowledge, recipients of
knowledge, the relationship of transfer parties, and the environment of
transfer''. The framework also includes ten key factors such as ``ambiguity,
systematism, transfer willingness, the capacity of impartation, the capacity of
absorption, incentive mechanism, culture, technical support, trust, and
knowledge distance''. The authors found that a trust
relationship among the involved teams influenced the knowledge transfer. They
conclude that the incentive mechanisms impacted positively in the
knowledge transfer regarding SPI. Both findings are something we also observed, 
as we discussed later on.
%
\cite{foods2006} studied key-relationships that influence the transfer of
knowledge between two partners. They collected both qualitative and
quantitative data and identified that ``trust, early involvement, and the due
diligence'' leverage the transfer of technology and tacit knowledge. The study
also evidence that managers and project leaders  comprehend poorly the implicit
knowledge transfer, content, and process. The authors found there are different
perceptions of knowledge transfer and a lack of methods to manage it. The findings
indicate that managers did not succeed in transferring the knowledge for
long-term product management, unlike what we report in this paper.
% TODO: maybe emphasize why that experience failed in the authors words
% so that can be compared later with why the experience being reported
% succeeded.

This paper focuses on two particular partners: academia and
government. Some works discuss how academia can collaborate and transfer 
knowledge to the industry in the management of software projects.
\cite{cho2011gap} evaluated the increasing use of agile techniques in software
development companies in Thailand.  The authors suggested that universities
should create curricula that develop in their undergraduate students' practical
skills required by industry (mainly agile practices) to promote growth in local
software businesses.  \cite{sandberg2017iacollaboration} report the use of
Scrum in an industry-academia research consortium (involving ten industry
partners and five universities in Sweden). Through a case study, they
demonstrate that being able to bring together the meaningful activities of the
stakeholders is essential to the success of collaborative research between
industry and academia.

%TODO(by Melissa): Abaixo está se misturando as temáticas introdução de métodos ágeis, inovação e colaboração.
Complex and large-scale organizations, such as the public administration, have to
deal with multiple project variables. In public administration, software
products are acquired or developed in a rigid framework of contract
obligations. According to \cite{mergel2016}, this factor helped to increase
``complexities, delays or even failed delivery of digital services''. As one of
the responses for that, government agencies are adopting agile methods
\citep{balter2011,margetts2013} ``to update large-scale legacy systems and adapt
to environmental changes and citizen requests faster''~\citep{mergel2016}.
Based on this context, \cite{mergel2016} conducted a study including interviews
with managers of the central digital transformation team from the U.S.
government. The author investigated the existing initiatives for agile
management implementation and proposed a research framework to guide future
investigations regarding collaborative and agile innovation management approaches in
government. This framework includes research questions such as: ``How can
government incentivize open sharing of source code instead of reinventing the
wheel with every request for proposals, signed contract or grant?''. The SPB
Portal, the target of our study, is a case study that answers this question.

Still in the U.S. government scenario, \cite{alleman2003making} describe a
production deployment focusing on the methodology applied to address long-term
planning and value estimation. Similar to them, we also embedded in our
environment an agile approach in a broader context.
%
In the Brazilian software development ecosystem, \cite{melo2013agileBr}
investigate the growing adoption of agile methods in the country's IT
industry. The results of their survey highlight some mismatch that companies
face when developing software for public administrations. In conclusion, the
study shows that the acceptance of agile methods has changed in the last decades.
The software development industry claims to follow some of the recommendations
of the agile manifesto, but some universities and companies are still resistant
to adopt such methods.

Discussions on how to introduce new management methods into an organization are
present in the literature~\citep{qumer2008,misra2009,mishra2011}.
%
However, \cite{nuottila2015} highlight that only a few works investigated agile
adoption in public organizations. Based on a case study from the Finnish
government, they conducted research ``to identify and categorize the
challenges that may hinder efficient adoption and use of agile methods in
public IT projects''. The study presents seven categories of challenges:
Documentation; Education, experience and commitment; Stakeholder communication
and involvement; Roles in the agile set-up; Location of the agile teams;
Legislation; Complexity of software architecture and system integration. These
challenges are directly related to the decisions discussed in our paper.
%
From a general point of view, \cite{nerur2015challenges} recognized critical
issues concerning the migration from traditional to agile software development
by comparing practices of both methodologies. The authors point out managerial,
organizational, people, process, and technological issues to be rethought and
reconfigured in an organization for a successful migration. They concluded that
agile methodologies are more suitable in projects with a high variation of
requirements, technical capacities and technologies, and formal and
bureaucratic organizations have more difficulty in the adoption of such
methods.
%
\cite{impactOfOrganizationalCulture} investigated the relationship between the
adoption of agile methodologies and organizational culture by evaluating nine
projects. They identified a set of six factors directly linked to agile methods
and concluded that the presence of these aspects in an organization is
proportional to the value of agile methodologies usage for their projects.
% TODO: maybe expand on what those six factors are, and relate them to
% this paper
%
Both works discuss that the adoption of agile development techniques does
indeed produce changes in an organization's
culture~\citep{nerur2015challenges,impactOfOrganizationalCulture}. Conversely,
our study does not discuss changes in the involved Brazilian government agency,
since the strategy reported here had the premise that both organizations
(academia and government) should collaborate with minimum impact on their
internal macro-management processes and culture.

Several works tried to highlight FLOSS practices, while others attempted to
determine the relationship between FLOSS practices and agile methods.
\cite{raymond}, in a seminal essay, described FLOSS best practices from
observations of the Linux kernel project and also reflections of his experience
with the FLOSS communities. \cite{capiluppi} examined about 400 projects to
find FLOSS project properties. In their work, they extracted generic
characterization (project size, age, license, and programming language),
analyzed the average number of people involved in the project, the community of
users, and documentation characteristics. \cite{warsta} found differences and
similarities between agile development and FLOSS practices. The authors argued
that FLOSS development might differ from agile in their philosophical and
economic perspectives; on the other hand, both approaches share the definition
of work. In its turn, \cite{fraser2006} claimed that FLOSS is a kind of Agile
software development methodology.

The studies regarding the relationship between FLOSS and Agile methods are
frequently concerned with managing the software project~\citep{gandomani2013}.
\cite{okoli2012} explained this relationship by comparing the characteristics
of FLOSS and Agile approaches, while \cite{magdaleno2012} stated that the
relationship between FLOSS and Agile practice is still embryonic. In the same
direction as our work, some studies reported that FLOSS and Agile share similar
values~\citep{adams2009,tsirakidis2009,corbucci2010,gandomani2013}. For
instance, both approaches rely on self-organized teams and shared goals in team
inputs~\citep{adams2009, tsirakidis2009} -- identical to what we observed in
our findings. \cite{gandomani2013} reviewed 27 research papers and found works
regarding case studies that describe the use of both methodologies
simultaneously~\citep{during2006, goth2007}, the same approach analyzed in our
research. Some studies pointed out that FLOSS and Agile methods could support
each other in some practices and tracking the progress of the software
development project~\citep{guido2007,deshpande2008,adams2009,
lavazza2010,gary2011,okoli2012,rahman2018}.

Nevertheless, none of these works provided enough evidence about the
comprehensive integration of FLOSS and Agile when used simultaneously in a
project~\citep{gandomani2013}. This issue is something we advance in the
current paper, investigating a large-scale complex case, compared to those
reported by~\cite{harzl2017} and ~\cite{muller2018}, for example. These recent
works present how FLOSS projects can apply agile management frameworks such as
Scrum or Kanban to favor software development planning.

Our work differs from all others discussed in this section by studying a
government-academia collaboration for developing a production-level solution.
We analyzed the decisions made during the life cycle of a real project from the
FLOSS and agile perspectives. Our findings are in line with the concept of
``continuous activities'', which together is part of the roadmap for continuous
software engineering proposed by \cite{fitzgerald2017}. Based on
questionnaires, interviews, and development activities data, we extracted the
(continuous) best practices that helped to harmonize the interactions between
two different development processes, as well as, satisfied the management
processes of government and academia sides.

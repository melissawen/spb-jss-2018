\section{Introduction}
\label{sec:Intro}

The use of information and communication technologies (ICT) to support
government procedures and routines, engage citizens, and provide government
services \citep{scholl2003}, known as Electronic Government (e-government), has
become synonymous with the idea of a modern state. While ``e-government'' has
different meanings for different people \citep{chandra2006}, such projects
virtually always differ from most other ICT projects due to their complexity
and large size \citep{anthopoulos2016egovernment}. On the one hand, they are
complex because they combine innovation, information \& communications
technologies, politics, and social impact. On the other hand, they are large in
their scope, target audience, organizational size, and time.

The development of innovative e-government projects that meet the needs of
society may be addressed through collaboration between government and academia.
However, managing such collaborative projects is often challenging. Indeed, one
of the leading causes of e-government project failure is poor project
management \citep{anthopoulos2016egovernment} caused by difficulties such as
lack of creativity and vision, poor communication and organization skills,
unclear work-breakdown, ineffective workload management, poor scope definition,
and change management, shifting requirements, technical complexity, and poor
delegation and tracking. In a joint project between government and academia,
other specific challenges become significant, such as organizing the
collaboration around the project, aligning goals, synchronizing the pace of
both partners, and overcoming the failure trend of e-government projects
\citep{goldfinch2007pessimism}. Therefore, proper management of the
collaborative project should be a relevant concern when government and academia
combine efforts to develop an e-government solution.

In the context of software development projects, an institution adopts
development methods that best meet its managerial procedures and organizational
culture.  For example, academia can work on cutting-edge development
methodologies while the government typically relies on traditional techniques.
When two large-scale, complex organizations decide to develop a solution
collaboratively, the development methods and workflow of one may conflict with
the culture and interests of the other.  Changing the development process of
either of them represents an organizational disturbance with impacts on
structure, culture, and management practices \citep{nerur2015challenges}.
Accordingly, when in collaboration, government and academia should strive to
increase the chances of success, given the typical tight deadlines and limited
outlay of such projects. Thus, they should avoid interferences in their
respective managerial processes that result only in negative impacts on the
project schedule and in budget wastage. Actual mechanisms to achieve this,
however, are not straightforward.

Difficulties such as these are part of the landscape of many FLOSS
(Free-Libre-Open Source Software) projects.  The development of such projects
is frequently a collaborative work involving multiple institutions with
different interests and organization without their internal processes being
affected. The significant success of FLOSS, which has considerable overlap with
agile methodologies, to deliver complex and quality products, reinforces the
power of applying their values and practices in collaborative scenarios.
Understanding how to overcome conflicting differences between crucial
stakeholders could lead a collaboration to success, especially in the case of
government and academia. 

Concerned with understanding the best practices that government and academia
can adopt to develop high-quality, well-structured, resource-saving
e-government solutions jointly, we focus on the following two research
questions:

\textbf{RQ1. }\textit{How to introduce FLOSS and agile best practices into
government-academia collaboration projects?}

\textbf{RQ2. }\textit{What FLOSS and agile practices favor effective team
management in government-academia collaborative projects?}

We explore these questions based on a case study about the SPB (Brazilian
Public Software, e.g., Sofware Público Brasileiro, in Portuguese). The project
behind it was an unprecedented 30-month software development structured as a
partnership between an academic team and a government agency aiming at three
goals: the development of a novel product (the SPB Portal); the acquisition of
experience and expertise regarding agile and FLOSS best practices within the
governmental agency; and the professional and educational evolution of the
involved undergraduate students. As we will see, this project exhibited many of
the complicating characteristics of government-academia co-development
projects. It enabled us to showcase both quantitative and qualitative analyses
of the benefits of FLOSS and agile practices.

To this end, we identified the applied best practices from FLOSS ecosystems and
agile methodologies used in the project. We collected and analyzed data from
the project repository and conducted a survey targeted at the project
participants to extract their perception about how these practices were useful
for government-academia collaboration. From this data, we identified three
high-level project management decisions that improved the project performance
and contributed to the success of the partnership: (1) the use of the system
under development to develop the system itself; (2) bringing together the
government staff and the development team; (3) organizing the development team
into priority fronts, and for each one, hire at least one specialist from the
IT market. Starting from these decisions and the perceived benefits brought by
them, we proceed to map how actual day-to-day practices materialized them. By
identifying them, we aim to help academia to understand better critical issues
they will be confronted with when engaging in a government-academia software
project.

This paper is an extension to previous studies from our research group
concerning a Brazilian government-academia collaboration project.  The first
study about the SPB portal development project~\citep{meirelles2017spb} was an
experience report that revealed lessons learned by the academic members
responsible for coordinating project activities (professors and senior
developers). In the second, \cite{siqueira2018} focused on presenting its
continuous delivery approach, technically describing the pipeline created to
support large organizations in the development of a system of systems. In a
more recent conference paper, \cite{wen2018gacolab} examined the method
developed over the 30 months of the project using both agile values and
standards from the FLOSS community to mitigate the cultural gap between
organizations.

Here we provide a comprehensive view of our previous studies on
government-academia collaboration. We go beyond the earlier works by
systematically investigating and describing how the practices adopted favor a
government-academia collaboration. Moreover, we discuss reflections from a
complementary questionnaire sent to participants and provide critical analysis
of the benefits and consequences of the decisions and the adopted practices.
The novel research resources enabled us to: (i) find new insights on software
engineering training; (ii) reach additional benefits that improve
collaboration; and (iii) identify how the experience with the adopted practices
has been reflected in the participants' professional skills a few years after
the end of the project.

The paper is organized as follows. Section~\ref{sec:background} addresses
underlying concepts and research opportunities related to this study, such as
co-development, knowledge transfer, and adoption of management practices in both large
organizations and public administration.  We discuss the
characteristics and associated difficulties of collaborative product
development, especially academia-government partnerships and e-government
endeavors, and how FLOSS and agile methods may be suitable in such scenarios.
%
Section~\ref{sec:spbcase} presents the object of our case study: the SPB
project.  We describe its origins and goals, the components that comprise it,
and the teams and organizations involved.
%
In Section~\ref{sec:ResearchDesign}, we state our research questions, focused
on the use of FLOSS and agile techniques to support government-academia
collaboration, provide a more detailed overview of the SPB project, and
describe how we collected and organized the analyzed data, which culminated in
the identification of the decisions, practises, and benefits that constitute
the core of this work.
%
Section~\ref{sec:results-discussion} offers a systematic view of the obtained
results, detailing the high-level project management decisions and the adopted
practices of the SPB project as well as mapping their benefits to the
collaboration as a whole, and discusses their consequences to the involved people.
%
In Section~\ref{sec:RelatedWork}, we discuss how this research coincides with
and differs from previous works, presenting the results reported in the
related literature and contrasting them with our findings.
%
Finally, in Section~\ref{sec:conclusion}, we summarize the material,
highlighting its main contributions, pointing out some of its limitations,
and suggesting paths to future works.

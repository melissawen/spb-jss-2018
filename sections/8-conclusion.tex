\section{Conclusion}
\label{sec:conclusion}

We reported on our investigation on the rebuilding of the Brazilian government
platform for software sharing, the SPB portal. For restructuring the
architecture and providing new functionality to the system, the federal
government teamed up with universities in a 30-month collaboration project. The
partnership developed a unique platform, described by project members
interviewed as an innovative product, with no other similar reference within
the Brazilian government. Due to the adoption of agile and FLOSS practices and
collaborative development technologies, the software delivered can be easily
updated and replicated for other government sectors, and the management
structure may serve as an example for future government-academia
collaborations.

In addition to technical aspects of the platform, the collaboration studied in
this work had a significant managerial complexity. By involving large-scale
organizations that have significant differences in their development methods
and customary workflows, the development process needed to be well-adapted to
mitigate mismatch and conflicts of interest, without interposing their internal
managerial procedures and organizational routines. This conciliation is crucial
in a case of government-academia collaboration since the weak and unadaptable
management could lead the project to fail, resulting in the waste of tax-payer
resources.

The case study enabled us to systematize management best practices that
mitigate the interinstitutional conflicts and led the partnership to succeed.
As a result, we identified high-level decisions, empirically taken by the
development leaders, which oriented the whole team to adopt practices from
agile methods and FLOSS ecosystems. From this, we asked members on both sides
of the project how these decisions had influenced the progress of activities.
The answers obtained, together with data collected from the development
repositories, enabled us to map the best practices and the benefits they
generated.

Regarding our first research question \textit{``How to introduce FLOSS and
agile best practices into government-academia collaboration projects?''}, we
examined the SPB project and identified three high-level decisions taken by the
academic coordinators that drove them to intuitively adopt nine FLOSS and agile
best practices in the development process:

\begin{description}

\item[D1] Use of the system under development to develop the system itself.
\item[D2] Bring together government staff and development team.
\item[D3] Organize development teams into priority fronts, and for each one, hire
at least one specialist from the IT market.

\end{description}

The interview responses and data collected from the development repositories
enabled us to understand how FLOSS and agile practices contributed to project
management. We recently sent ex-interns a set of complementary questions to
update our analysis of the adopted management method benefits and the project
experience impacts on their professional lives.  Based on these data, we
answered the second research question \textit{``What FLOSS and agile practices
favor effective team management in government-academia collaborative
projects?''}, by enumerating at least 19 benefits obtained from the use of the
nine best practices:

\begin{description}

\item[P1.1] The features and tools of the platform under development support the project management and communication activities.
\item [P2.1] Government staff, academic coordinators, senior developers, and team coaches meet biweekly at the university lab for sprint planning and review.
\item [P2.2] Direct contact, with no bureaucratic barriers, between government staff and the development team on the platform technical discussions.
\item [P2.3] Involve government board of directors only in strategic planning of the project.
\item [P2.4] Build a Continuous Delivery pipeline with stages involving both sides.
\item [P3.1] Coordinators separate the development team into priority work areas considering the main demands of the project.
\item [P3.2] IT market professionals with recognized experience on each front were hired to work in person or remotely.
\item [P3.3] Identify among the interns the leadership roles: a coach for each front, and a meta-coach of the entire development team.
\item [P3.4] Each team develops its self-organization, being guided by one intern-coach and at least one senior developer.

\end{description}

Moreover, based on the responses from the second questionnaire sent to the
ex-interns, we were also able to evaluate the additional benefits in terms of
training in software engineering via an experience on a real software
development project. Interns were exposed to the complexities of large-scale
institutions, advanced high-tech challenges, as well as the dynamics of FLOSS
projects. About two years after the end of the project, the ex-interns who
participated in the new SPB portal development reached significant positions in
their academic or professional careers; several of them also have significant
contributions to large FLOSS projects. Our study showed that the adopted
practices impacted the people involved in the project notably.

This research has a few limitations. First, we point out the lack of formal
communication records and low traceability of the management data referring to
the first phase of the project. Second, we consider a drawback the hiatus
between the completion of the project and the execution of interviews and
questionnaires, since we relied on the memory of the interviewees to recollect
events. Nevertheless, this hiatus also allowed team members more time to
reflect on their participation in the project and let them gain more
professional experience enabling them to better evaluate the period in which
they participated in the project.  Third, the current situation of the
respondents, such as their current working mindset, may also alter their
perception of the topics addressed in the questionnaire and, consequently,
their responses. Finally, the decisions, practices, and benefits discussed
should be evaluated and used in context with a more substantial plurality and
diversity of government stakeholders.

We believe that our findings can be used as effective guidelines for
govern-ment-academia collaborative software development projects. In fact, UnB
and USP are currently adopting some of these practices to develop
government-academia software projects in the fields of Healthcare and Culture
in collaboration with federal and state governments.  As future work, we
believe it will be valuable to compare different approaches for managing
government-academia collaborations and map other useful managerial methods,
evaluating the short- and long-term organizational impacts on the institutions
involved.

\section*{Acknowledgments}

This research is part of the INCT of the Future Internet for Smart Cities
funded by the Brazilian National Council for Scientific and Technological
Development (CNPq) proc. 465446/2014-0,  the Coordination for the Improvement
of Higher Education Personnel -- Brasil (CAPES) -- Finance Code 001, and the
São Paulo Research Foundation (FAPESP) proc. 14/50937-1 and proc. 15/24485-9.
Some authors were supported by fellowships from CNPq and CAPES, Brazil.
The authors thank especially all project ex-interns and IT professionals as well as
the Brazilian government IT professionals for answering the questionnaires,
participating in the interviews, and being available to improve our
understanding of the SPB rebuilding project. Their contributions during and
after the project were essential for developing this research.

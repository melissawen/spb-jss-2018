\section{Results and Discussion}
\label{sec:results-discussion}


As previously shown in Figure \ref{fig:methodology}, we extracted a set of high-level decisions
presented as beneficial to the project from the lessons learned reported by
academic coordination in the first study on the
project~\citep{meirelles2017spb}. Based on the material just discussed, we
analyzed the data obtained to understand the managerial practices, their
consequences for the project, and to classify practices and benefits in a more
consolidated description of the high-level decisions.
%
This analysis resulted in an organized view of the core decisions made in the
project, the practices that embodied these decisions throughout its evolution,
and the perceived benefits brought by them.

\input{sections/decision-practise-benefit-table.tex}


Table~\ref{practices-table} summarizes the results, organizing them in three columns. The first one presents the high-level decisions identified by the letter ``D'' plus the number of the decision (e.g., ``D1''). The second column shows the managerial practices related to each decision. Its identifier includes the letter ``P'' plus the numbers of the related decision and the practice (e.g., ``P1.1''). The last column presents the benefits derived from each decision and its associated practices. We labeled its identifier with the letter ``B'' plus the numbers of the related decision and the benefits (e.g., ``B1.1''). We will use these identifiers to refer to the decisions, practices, and benefits in several parts of the discussions of the results in this section.


\subsection{Decisions and Benefits}
\label{sec:results}

In the case study, we identified three high-level project decisions that
were relevant to reduce the communication gap between the teams and to
increase project productivity:

\myparagraph{D1} Use of the system under development to develop the system itself;

\myparagraph{D2} Bring together government staff and development team;

\myparagraph{D3} Organized development team into priority fronts, and for each one, hire at least one specialist from the IT market.

We now describe each of these
decisions and their benefits according to the respondents, and present
an analysis of corresponding repository data.

\subsubsection{Decision (D1):  Use of the system under development to develop the system itself}
\label{sec:d1}

The new SPB Portal combines functionalities adapted from existing
collaborative software and others developed by the project team. As a result,
the platform integrates different features such as social
networking, mailing list, version control system, content management, and
source code quality monitoring.

Because of these features, the development coordinators decided to use the platform
under construction to develop the system itself. Gradually, in addition to
development activities, the government and university teams moved the project management
and communication between them to the portal environment.

Features like help pages, mailing list management, project-centered
administration, Mezuro code quality analysis, and global search, offered
the expected practical support for the development process. This use was
also instrumental for the ongoing testing and improvement of the
platform.

\mysubsubsub{Collaborative Development Features supporting D1}


The SPB Portal became a platform to stimulate (1) the openness of the source
code, (2) an ongoing dialogue between users and the development team, and also (3) maintenance and
evolution of the software, which provide more transparency in Government
investments regarding software development. These qualities were offered
mainly by use of the Collaborative Development Environment tools inherited from
the GitLab integration.

Usually, CDEs demand a version control
system, trackers, build tools, knowledge centers, and communication tools \citep{lanubile2010}.
The new SPB Portal also provides tools to encourage
developers to keep the source code and its development activity within the
platform. Any software project created in the SPB Portal has, by default, an associated Git repository
with Wiki pages and issue tracking. The tools most often used during the
development of the new SPB were:

\begin{enumerate}
  \item Project repository: the teams used one organization with many repositories;
  \item Wiki: each release had one Wiki page with the compilation of the
    strategical meeting notes;
  \item Milestones: each milestone was used to register a user story (feature);
  \item Issues: each sprint planning generated issues, which were associated to
    the related milestone (feature as user story) and registered on the corresponding Wiki page.
    Finally, each developer assigned the issue to him or herself.
\end{enumerate}

The features provided by GitLab were particularly crucial in the management of the
project. In short, the wiki feature was used for logging meetings, defining goals,
planning sprints, and documenting deployment procedures and user guides. The issue
tracker supported discussing requirements, monitoring features under
development, requesting and recording changes, and validating the delivered
functionalities. Finally, the mailing list was used for the collaborative
construction of requirements, defining schedules, and scheduling meetings
between institutions.

Thanks to the variety of features present in the platform, it was easily adapted to
manage project planning, documentation, and development. Both university and
government teams navigated and made use of these features daily. In general,
the assimilation of the created functionalities happened organically, and the
constant use of the development tools enabled the project members to test and
validate their correct operation from the view of the developer, the user,
and also the client.

\mysubsubsub{Benefits brought by D1}

Our surveys report the mailing list (100\%) and issue tracker (62.5\%) as the primary
means of interaction between senior developers and interns. The development
team and MPOG staff also interacted mostly via the mailing list (87.5\%) and issue
tracker (50\%). According to one of the interviewees, this decision made the
\textbf{communication more transparent and efficient} (B1.1). An MPOG analyst said
that:

\begin{quote}
\textit{``Communicating well goes far beyond speed. It means enabling
someone to tell everyone about everything that is happening in the project. We
did not use [private] emails, we use more mailing list and avoid [private] emails. This usage
helped us considerably. Everything was public and did not pollute our email
box. So, when you wanted to know something, you could access the SPB list [on the web] and
see everything''}.
\end{quote}

\label{decisions:repo-discussion}
Migrating to the SPB platform also \textbf{eased monitoring of activities} (B1.2) and
\textbf{increased interactions between developers and public servants} (B1.3). The data
collected from the repository (Section~\ref{sec:descricao-repo}) highlights the
frequent use of the platform by both teams.

Significant usage by the development team was to be expected and, indeed, they
made 3,256 commits in the repository mentioned above.
More importantly, there was strong participation from the government team
as well: from April 2015 to June 2016 (the last 15 months of the
project), 59 distinct authors, 8 of which were MPOG agents, opened 879 issues.
These issues received a total of 4,658 comments from 64 distinct users, 9 of
them from MPOG. When we consider the issues with more interactions --- those
which had ten comments or more ---, we
notice that the government team also felt comfortable in using the tool to
interact directly with the development team. In a set of 102 active issues,
MPOG staff created 43 of them (this represents 42\% of the most active issues).

For the MPOG analysts, interaction via repository improved communication:

\begin{quote}
\textit{``There was a big evolution, we increased our communication via
GitLab''}.
\end{quote}

Migrating to the platform also led MPOG staff to \textbf{trust the
developed code} (B1.4):

\begin{quote}

\textit{``Everything was validated.  We tested the
functionalities and developed the project on the SPB platform itself. Hence,
the use of the system validated most of its features. From the moment we
began to use it for development, this validation was constant. We felt confident
in the code produced''}.

\end{quote}

The decision mentioned above also collaborated to meet the government's demand
for meticulous documentation of the software design and stages of development
without bureaucratizing or modifying the development process. The usage of the
platform for project team management conducted the \textbf{organic production
of documentation and records} (B1.5), as mentioned in one of the MPOG responses:

\begin{quote}
\textit{``It was a great learning experience. There are many things documented
in emails as well as in the portal itself. We can access the tools at any time
and find out how we developed a solution. We can remember the positive points''}.
\end{quote}

%===============================================================================

\subsubsection{Decision (D2):  Bring together government staff and development team}
\label{sec:d2}


As the project evolved, the MPOG analysts that we interviewed became responsible for
negotiating with the teams the day-to-day development and product delivery
decisions as well as for reporting to their superiors on
inter-institutional meetings. During the first phase of the project, they
did not participate in any direct interaction with university representatives.
They stated that at that time there was significant communication noise in the
internal dialogues with their superiors, as well as between them and the
development team. It was in the project's second phase that those analysts
became direct government representatives and started to visit the
laboratory at the university biweekly. At this time, the development workflow
had also changed due to the implementation of a Continuous Delivery (CD) pipeline. The
development team used CD as a survival technique, that is, as a way to gain the
government trust~\citep{siqueira2018}.

\mysubsubsub{Continuous Delivery Pipeline supporting D2}

The project's deployment followed a typical CD pipeline \citep{humble2010},
adapted to the project's technical and organizational context and the use of FLOSS best
practices. As depicted in Figure~\ref{fig:pipeline}, it began when a new feature
is ready for deployment and ended when it reached production.

\begin{figure}
  \centering
    \includegraphics[width=\linewidth]{figures/pipeline.pdf}
  \caption{The SPB Deployment Pipeline}
  \label{fig:pipeline}
\end{figure}


The implemented code was heavily tested. Each integrated system had its own test
suite, and there was also a test suite for the platform as a whole. If any test suite
failed, by either a test error or coverage reduction below a certain threshold,
the process stopped. Only when all tests passed, the pipeline could proceed to
prepare a new release.

SPB had its own Git repository\footnote{\url{https://softwarepublico.gov.br/gitlab/softwarepublico}}.
An SPB portal release was an aggregate of all its integrated systems. That is,
when one of them passed all of the SPB integration tests, the team manually created a
corresponding tag on the SPB repository. At the end of this process, the DevOps
team was ready to start packaging.

Packaging brings portability, simplifies deployment, and enables configuration
and permission control. The approach used involved building separate packages for
each system, in three fully automated steps: generating scripts for the
specific environment, building the package, and uploading it to a package
repository. When all ran successfully, the new packages would be ready and
available for the deployment scripts. Before deploying to production, the
system would first be deployed to a validation environment.

The Validation Environment (VE) was a replica of the Production Environment (PE)
with anonymized data and access restricted to MPOG staff and the DevOps team.
%
After a new SPB portal VE deployment, the development team used the environment to verify the
integrity of the entire portal. MPOG staff also checked the new features,
required changes, and bug fixes. If they identified a problem, they would
notify developers via comments on the SPB portal issue tracker, prompting the
team to fix it and restart the pipeline. Otherwise, the team could move forward to
production deployment, using the same configuration management tool, scripts,
and package versions as in the VE. After the deploy was completed, both VE and PE
were identical, and any new features and bug fixes would become available to end users.

\mysubsubsub{Benefits of D2}

CD brings many advantages such as accelerated time to market, building the
right product, productivity and efficiency improvements, stable releases, and
better customer satisfaction~\citep{chen2015, savor2016}. Figure
\ref{fig:releaseandteamevolution} shows the evolution of the number of releases
during the project's lifetime both regarding its growth over time, grouped per semester (red bars) and depicts the number of project members (green line) and the reorganization of the team for the creation of a group specialized in DevOps (blue line).

Over 30 months, 84
versions were deployed. Without taking into account the first half of the
project, where the team and the project requirements were still under
construction, the project deployment averaged 15 versions per semester. After the
creation of a DevOps team and the implementation of continuous delivery in June
2016, the number of deployments per semester grew to 27, representing an
increase of 80\% on the delivery rate. In summary, over 64\% of the total of
releases happened in these last 12 months of the project. It is also important to note that
there is an intersection between DevOps members and undergraduate interns.
Again, it is clear that, after an initial period dedicated to building and
improving the CD pipeline, the creation of the DevOps team was
crucial for the project to make more frequent releases.

\begin{figure}
  \centering
    \includegraphics[width=\linewidth]{figures/CDReleaseAndTeamEvolution.pdf}
  \caption{The evolution of the SPB release in comparison to development team members distribution}
  \label{fig:releaseandteamevolution}
\end{figure}

Respondents also reported CD benefits. For 30 out of 37 (81\%) of the interns and
for 6 out of 8 (75\%) of the IT professionals, deploying new versions of the SPB
portal in production was a motivator during the project. On the government side,
this approach helped to \textbf{overcome the government bias toward low productivity
of collaborative projects with academia} (B2.4), as mentioned by them:

\begin{quote}
\textit{``Government staff has a bias that universities do not deliver
products. However, in this project, we made many deliveries with high quality.
Nowadays, I think if we had paid the same amount for a company, it would not
have done the amount of features we did with the technical quality we have''}.
\end{quote}

Additionally, constant deployments enabled both side teams to \textbf{share a
common understanding of the process} (B2.8), because MPOG analysts became able to keep a
high-level view of the project and to focus on planning future releases, as one
of them mentioned:

\begin{quote}
\textit{``We had only the strategic vision of the project. When
we needed to deal with technical issues, we had some difficulty planning the
four-month releases.  However, in the last stages of the project I realized
that this was not a problem. The team was delivering and the results were
available in production.  The team was qualified, the code had quality, and the
project was well executed.  So in practice, our difficulty in interpreting the
technical details did not impact the release planning''}.
\end{quote}

The presence of government representatives in the university laboratory was
also responsible for reinforcing the government-academia synchronization. One of
the analysts believed that:

\begin{quote}
\textit{``At this point, the communication started to change''}.
\end{quote}

The new dynamics \textbf{reduced communication misunderstandings} (B2.1) and
unified both sides, as reported by another
interviewee:

\begin{quote}
\textit{``It was very positive. We liked to go there and to
interact with the team. I think it brought more unity, more integration into
the project''}.
\end{quote}

27 out of 37 (73\%) of the interns considered positive the direct
participation of the MPOG staff, and 30 out of 37 (81\%) of them believed the presence of
government staff in sprint routines was relevant for the project development.
For 28 out of 37 (76\%) of the interns, writing the requirements together with the MPOG staff
was very important to \textbf{meet expectations of both sides better} (B2.2).
According to one of them, this closer interaction \textbf{improved the
decision-making process} (B2.3):

\begin{quote}
\textit{``Joint planning and timely meetings were very important for
understanding the needs of MPOG''}.
\end{quote}

Closer dialogue between government and academia generated empathy, as
reported by one of the interviewees:

\begin{quote}
\textit{``Knowing people in person makes a
big difference in the relationship because it causes empathy. You know who that
person is. He's not merely a name''}.
\end{quote}

Consequently, this empathy helped to \textbf{synchronize the execution pace of
activities} (B.5):

\begin{quote}
\textit{``Visiting the
lab and meeting the developers encouraged us to validate resources faster and
give faster feedback to the team. In return, they also quickly answered us any
question''}.
\end{quote}

Some of the ex-undergraduate interns currently serve as academic professors or
government employees. Due to the proximity between government and academia
experienced during the project, such students reported encouraging the
public administration to adopt agile methods, software sharing, and
collaborative software development:

\begin{quote}
\textit{``The project experience developed my view about the value of FLOSS and
software sharing between public administration departments. Hence, I have
brought this concern inside the execution of my assignments and in discussions
related to this theme with others civil servants.''}
\end{quote}

They also believe that these values, when implemented at the governmental
level, enable resource savings and better project planning:

\begin{quote}
\textit{``The project enabled me to acquire a practical understanding of software development using agile methodologies and taught me the correct use of tools which bear this kind of development. Through this knowledge, we (MPOG) are spreading to other departments, and public servants the importance of agile methods and collaborative development for resource saving and software development planning inside the ministry.''}
\end{quote}

%===============================================================================

\subsubsection{Decision (D3): Organized development team into priority fronts, and for each one, hire at least one specialist from the IT market}


The SPB team was composed of a variety of professionals with different levels of experience
and skill, where most of them were undergraduate software
engineering students. Since students could not dedicate many hours per week to the
project, they had the flexibility to negotiate their work schedule during the
semester in order not to harm their classes and coursework. Their work routine
in the project included programming and DevOps tasks.

The project required a vast experience and background that
undergraduate students do not usually have. For this reason, a few senior developers
have joined the project to help with the more difficult issues and to transfer
knowledge to the students. Their main task was to provide solutions for complex
problems, working as developers. As these professionals were very skillful and
the project could not fund full-time work for all of them, they worked
part-time on the project. Besides, they lived in either different Brazilian
states or other countries, which led much of the communication to occur online.

\mysubsubsub{Team Organization adopted with D3}

Approximately 70\% of the development team was composed of software engineering
undergraduate students from UnB working physically in the same laboratory. The
senior developers tried to synchronize their work with the students' schedules,
but each student had his schedule based on his classes, which complicated the
adoption of pair programming. To cope with this scenario, a few basic rules
were guiding the project organization:

\begin{enumerate}
  \item Classes have high priority for undergraduate students;
  \item Pairing whenever possible (locally or remotely);
  \item During one morning or afternoon per week,
    \emph{everyone} but the remote members
    should be together physically in the laboratory;
  \item Every 2 to 3 months the senior developers would travel to work
    alongside the students for a few days.
\end{enumerate}

With the aforementioned rules, the development team had four work areas
divided in accordance to the main demands of the project: User Experience, DevOps,
Integration of Systems, and Social Networking. One student of each team was the
coach, responsible for reducing communication issues with other groups and
helping the members to organize themselves in the best way for everyone (always
respecting their work time). The coach also had the extra duty of registering
the current tasks developed in the sprint. One important thing to notice is
the mutability of the team and the coach. During the project many students
switched teams to try different areas.

For each segment, at least one professional from the IT market was hired to raise
the quality of the product assuming the position of senior developers. They
have been selected based on their vast experience in FLOSS systems and their
knowledge of the tools used in the project. Their expertise was essential to
address hard decisions and complex problems. Thus, it was not
the coach's role to deal with complicated technical decisions, which encouraged
students to be coaches. Lastly, the senior developers worked directly with the
students, and this was important to give them the opportunity to
interact with a savvy professional in their areas and to keep the knowledge
flowing in the project.

Finally, two other elements in the team organization were essential for the
project harmony: the meta-coaches and the professors. The former
were software engineers recently graduated that wanted to keep working on the
project. The latter were professors that orchestrated all the interactions
between all members of the project.  Each meta-coach usually worked in one
specific team and had the extra task of knowing the current status of all the
others. Professors and the meta-coaches worked together to reduce
communication issues among groups. Lastly, all the paperwork tasks, such as
reporting on the project progress to the Brazilian Government, was handled by
the professors.

\mysubsubsub{Benefits of D3}

The presence of senior developers in the project contributed to
\textbf{conciliate the development processes of each institution and make
better technical decisions} (B3.1), as quoted in one of the answers to the senior
developer's questionnaire:

\begin{quote}
\textit{``I think my main contribution was to balance the relations between the
MPOG staff and the university team''}.
\end{quote}

5 out of 8 (62.5\%) of the IT professionals believe they have collaborated to conciliate the
management and development process between the two institutions and another 5 of 8 (62.5\%)
helped MPOG staff express their requests more clearly. Government
analysts were also more open to suggestions from these developers:

\begin{quote}
\textit{``They are upstream developers of the systems that integrate the
platform. They conveyed trust, and therefore we trust the developed code''}.
\end{quote}

According to questionnaire responses, IT professionals mostly agreed with the
project development process. For 5 out of 8 (62.5\%), this process has close similarity to
their previous experiences. In contrast, another 5 of 8 (62.5\%) did not understand
the MPOG's project management process and 4 out of 8 (50\%) believed this
process could affect the project productivity.

The senior developers were also responsible for \textbf{improving the
management and technical knowledge} of the interns about practices from
industry and open source projects (B3.2). 34 out of 37 (92\%) of the interns believed that working
with professionals was essential for learning and, for all of them, working
with IT professionals was important during the project. 6 out of 8 (75\%) of the IT
professionals believed that ``Working in pairs with a senior'' and 5 out of 8 (62.5\%) that
``Participating in joint review tasks'' were the activities with the involvement of
them that most contributed to the evolution of the interns.
6 out of 8 (75\%) believed that the knowledge shared by them with one intern was widespread
among the others in the team.  Government analysts also pointed out this knowledge
sharing:

\begin{quote}
\textit{``On the university side, we noticed a significant improvement in the
platform with the hiring of the systems' original developers. They had a guide
on how to best develop each feature and were able to solve non-trivial problems
quickly''}.
\end{quote}

Organizing the development team and hiring the IT professionals allowed each
team to \textbf{self-organize and gain more autonomy in the management of their
tasks} (B3.5).  There was a development coach to lead each team, and at least one meta-coach
supported all of them in their internal management activities. The coaches
(most advanced interns) were important references in the development process.
33 out of 37 (89\%) of the interns said that the presence of the coach was essential to the
sprint's running, and for 7 out of 8 (88\%) of the IT professionals the coaches were
crucial for their interaction with the development team. MPOG analysts saw
the coaches as facilitators for their activities and communication with the
development team. They said:

\begin{quote}
\textit{``I interacted more with the project coordinator (professor) and team
coaches''}, \textit{``Usually, we contact a coach to clarify some requirements
or to understand some feature. The coaches were more available than senior
developers and, sometimes, they would take our question to a senior
developer''}.
\end{quote}

When we recently asked former undergraduate-interns about the experience
acquired during the project, we observed that the team's organization and the
presence of senior developers had brought maturity and positive impacts on
their current professional activities. One of them stated that:

\begin{quote}
\textit{``The SPB project experience offered me a great maturity on
professional FLOSS development''}.
\end{quote}

For another, the experience favored his career directly:

\begin{quote}
\textit{``The project experience helped me to develop myself technically and to
understand some issues about project management of which I wouldn't have the
opportunity in another moment. I also built a strong and valuable professional
network with very qualified developers, and one of the senior developers
introduced me to the Debian project (a Linux distribution). This gateway to the
FLOSS environment was crucial for my current job in a recognized company of
Open Source consultancy''}.
\end{quote}

Some respondents said that the experience with a
partially remote team also contributed to their professional growth. In general,
ex-interns have spontaneously reported that the contact of developers from
different areas and professional profiles has enabled them to improve their
technical skills and build a network of professional contacts:

\begin{quote}
\textit{``Besides the opportunity of technical contribution to large software
projects, I learned a lot with senior developers as well as undergraduate
colleagues. The project gave me the chance to work with various professional
profiles (such as designers) and introduced me to the FLOSS world wherewith,
and I am very grateful''}.
\end{quote}

One ex-participant also reported that the organization of the team into priority
fronts, and the possibility of migration between them, gave him a comprehensive
view of how a quality product is developed. The experience of the project as a
whole was the basis for some of them to be able to work directly on FLOSS
projects - such as cooperating to Noosfero project, participating and mentoring
in the Google Summer of Code Program, mentoring in the Outreachy
Program\footnote{\url{https://outreachy.org}}, and contributing to the Linux Kernel
project.

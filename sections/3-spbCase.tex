\section{The case of the Brazilian Public Software Portal}
\label{sec:spbcase}

The Brazilian government designed the original Brazilian Public Software (SPB)
portal in 2005 and released it in 2007. It is a web system that has
consolidated itself as an environment for sharing software
projects~\citep{freitas2008}. The SPB portal\footnote{\href
{https://pt.wikipedia.org/wiki/Software_livre_nos_governos\#Software_P\%C3\%BAblico_Brasileiro_(SPB)}
{\nolinkurl{https://pt.wikipedia.org/wiki/Software_livre_nos_governos\#Software_Público_Brasileiro_(SPB)}}
} also provides a space (community) for each software. These communities have
tools to promote collaboration and interaction among managers, users, and
developers, according to practices FLOSS communities use. It was created to
support the plan of the government to foster public adoption of FLOSS
solutions.

Initially, the portal's purpose was only to share software developed for or by
any of the many Brazilian government branches to reduce the costs of hiring
software in others. However, for every new software project released, a
community was formed around it. Several people, then, started collaborating and
sharing the results obtained through the use of those solutions, as commonly
occurs in FLOSS~\citep{ducheneaut2005}.

The concept of a Brazilian Public Software goes beyond
FLOSS~\citep{freitas2008}. In addition to being licensed under a FLOSS
license, the software needs to have explicit guarantees that it is a public
good and its project must be available on the SPB portal. Inclusion in the SPB
Portal also has extra requirements, such as having a public version control
system, installation manual and hardware requirements
specification~\citep{meirelles2017spb}.

Notwithstanding its value, in practice, in 2009 the SPB Portal started having
several technical issues. The original codebase development stopped, and the
SPB Portal did not receive any code updates. To recover the SPB portal, in
January 2014, the University of Brasília (UnB) and the University of São Paulo
(USP), in a partnership with the Brazilian Ministry of Planning, Budget, and
Management (MPOG), designed a project to evolve the SPB portal into an
integrated platform for collaborative software development
(CDE)~\citep{bobr2003} with additional social capabilities.  The new SPB Portal
was developed by partially distributed teams of undergraduate interns, IT
professionals and professors from the Advanced Laboratory of Production,
Research, and Innovation in Software Engineering
(LAPPIS/UnB\footnote{\url{https://lappis.rocks}}) and the FLOSS Competence
Center at USP (CCSL/USP\footnote{\url{http://ccsl.ime.usp.br}}), both with
experience in FLOSS development.

The new SPB portal\footnote{\url{https://softwarepublico.gov.br}} includes features
such as social networking, mailing lists, version control system, and source
code quality monitoring.  As of October 2018, it had 83 software communities
and 7,347 user accounts, mostly from government employees. It is a novelty in
the context of the Brazilian government, due to the technologies employed and
its various features. Moreover, these characteristics led the project to
interact with different FLOSS projects and communities. At the end of the
project, the teams built a system-of-systems~\citep{nielsen2015}, adapting and
integrating five existing FLOSS projects:

\myparagraph{Colab\footnote{\url{https://github.com/colab}}} is a system
integration platform for web applications. It provides a unified view and
interface for a set of different software systems. Thus, users do not notice
significant differences when they shift their interaction from one application
to the other. For that, Colab provides facilities for (i) centralized
authentication, (ii) visual consistency, (iii) relaying of events between
applications and (iv) an integrated search engine. Colab implements this
integration by working as a reverse proxy for the applications, i.e., all
external requests pass through Colab before reaching them.

\myparagraph{Noosfero\footnote{\url{https://gitlab.com/noosfero/noosfero}}} is a software for
building social and collaborative networks. Besides the classical social
networking features such as uploading content and enabling commentaries to make
conversations, it also provides publication features such as blogs and a
general-purpose CMS. Most of the user interactions with SPB is through
Noosfero: user registration, project and documentation, and contact forms.


\myparagraph{GitLab\footnote{\url{https://gitlab.com}}} is a web-based Git
repository manager with wiki pages and issues tracking features. It is a FLOSS
platform and focuses on delivering a holistic solution that will support
developers from idea to production seamlessly and on a single platform. GitLab
has several unique features, such as built-in continuous integration and
continuous deployment, flexible permissions, tracking of Work-in-Progress work,
moving issues between projects, group-level milestones, creating new branches
from issues, dashboard and time tracking.

\myparagraph{Mezuro\footnote{\url{https://mezuro.github.io}}} is a platform to collect
source code metrics to monitor the internal quality of software written in C,
C++, Java, Python, Ruby, and PHP. It provides a single interface grouping
available metric collection tools, allows the selection and composition of
metrics flexibly, stores the metrics evolution history, presents results in a
friendly way, as well as allows users to customize the given metric values
interpretation according to their context.


\myparagraph{GNU Mailman\footnote{\url{https://list.org}}} is an application for
managing electronic mail discussion and e-newsletter lists. Colab itself
provides a web interface for GNU Mailman to allow the dialogue and
communication between developers, users, and enthusiasts of a determined
software. Each software community has its mailing list whose privacy settings
can be configured by the software community administrators.


All these integrated systems involve a total of 106,253 commits and 1,347,421
lines of code. In summary, Colab orchestrates these multiple systems using a
plugin architecture and smoothly provides a unified interface to final users,
including single sign-on and global searches.  Without it, the integration of
the several features of these systems and other needed back-end features
written in different programming languages and frameworks would require a
non-trivial amount of work.

In this scenario, we studied the open and collaborative software development
practices that inspired the SPB project progress.  The academic teams
empirically defined an adaptation of different agile and FLOSS communities
practices to guide the development process, with a high degree of automation
resulting from DevOps practices and also a working process executed in a
cadenced and continuous way with the government team.

While the development itself was handled by the academic team, this was not a
typical outsourcing scenario:

\begin{itemize}

\item The academic partner had a voice in the strategic decisions, as well as
the requirements and features definitions;
	
\item Students in leadership roles participated in meetings with government
managers;

\item Much of the communication and validation of the project converged to the
GitLab tool within the project itself;

\item The group created a ``translation'' mechanism from the development team
project assessment to the government's managerial tools;

\item Budget management was a concern for both partners;

\item The adopted Continuous Delivery pipeline highlights the collaborative
aspect of the project workflow;

\item One of the goals of the partnership was to promote the transference of
knowledge from academia to the government staff regarding FLOSS and agile
practices.

\end{itemize}

In summary, during the SPB project, the academia and the government coordinated
and divided the responsibilities of the management activities, sharing concerns
about funding and accountability, and business rules and strategic decisions.

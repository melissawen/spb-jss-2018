\section{Related Work}
\label{sec:RelatedWork}

\cite{collaborationOverview:2012} divide the literature on collaborative
product development in three categories: papers that describe typical
collaborative product development dynamics and success factors, usually case
studies; papers that deal with the process of partnership formation, such as
choice of partner; and papers discussing technological and methodological tools
to improve collaboration.  Our work fits in the first and third categories, as
it is a case study that describes the progressive development and adoption of
new technologies and management techniques as the project evolved. Therefore,
in this section, we prioritize the literature dealing both with the adoption of
novel management approaches and with co-development involving either academia
or government.

\cite{dittrich2002} discussed the co-development of services,
citizenship, and technology in the e-Government context. From four different
cases, they mapped similar issues regarding e-government development. Since
e-government changes the paradigm of several public services, the authors claim
that it requires coordination by means of design and construction of supporting
technical and organizational infrastructures. Our findings show that the
government, especially the analysts, acquired all the expertise to design and
provide the technical and organizational infrastructure to the users of the new
SPB portal.  They were actively involved in the collaborative development
process and used the new platform tools for their management and communication
activities during project development. From the analysts' responses, we
observed that they recognized the potential of the new platform in practice
through the development of the project itself. Hence, they became advanced
users of the new SPB portal before any other users. Based on this experience,
they also increased their understanding of collaborative software development.

Co-development is also related to knowledge transfer.
\cite{wan2010} conducted an empirical study on the influence of knowledge
transfer in Software Process Improvement (SPI). They proposed a conceptual
framework composed of five elements: ``transfer of knowledge, sources of
knowledge, recipients of knowledge, the relationship of transfer parties, and
the environment of transfer''. The framework investigated ten key factors such
as ``ambiguity, systematism, transfer willingness, the capacity of impartation,
the capacity of absorption, incentive mechanism, culture, technical support,
trust, and knowledge distance''. Among these, the authors found that a trust
relationship among the involved teams influenced the knowledge transfer and
concluded that incentive mechanisms impacted positively in the knowledge
transfer regarding SPI. Both findings are something we also observed.  Our work
reports that continued delivery and joint participation by both sides at
various stages of the development cycle have built trust between government and
academia. This confidence aroused the interest of the development team in
understanding and better meeting the bureaucratic demands of the government.
Also, the government analysts recognized the importance of absorbing agile
values and FLOSS to the dynamism of the workflow.  In the responses to the
interviews and questionnaires, we also observed both teams realized that the
transparency and learning of different project management techniques made the
work more fluid and productive. Senior developers were not only motivated by
payment but also by the opportunity to get new contributors to evolve the
projects for which they were creators or maintainers. Besides, they found a way
to show the relevance of their area of expertise to customer satisfaction and
product acceptance by teaching students.

\cite{foods2006} studied key-relationships that influence the
transfer of knowledge between two partners. They collected both qualitative and
quantitative data and identified that ``trust, early involvement, and the due
diligence'' leverage the transfer of technology and tacit knowledge. The study
also evidences that managers and project leaders comprehend poorly the implicit
knowledge transfer, content, and process. The authors found there are different
perceptions of knowledge transfer and a lack of methods to manage it. The
findings indicate that managers did not succeed in absorbing knowledge for
long-term product management. Managers work based on well-defined schedule and
scope requirements of their projects, being ``rewarded on execution, timing,
and budgetary compliance''. \cite{foods2006} indicate that managers extract
from their partner ``minimally what is required to commercialize the product''
because they are not responsible for the ``next-generation product that they
may not be working on''.  Unlike what is claimed in that paper, government
analysts who managed the SPB project gained much knowledge from the academic
and development team. They incorporated several practices in their day-to-day
activities, involving other MPOG staff to collaborate in the SPB project. They
also reported that the availability of ample documentation from the project is
a valuable asset they can refer to at any time, suggesting a long-term change
in management strategies and knowledge. In fact, an analyst mentioned that the
experience extracted during the SPB project is useful to her new position as a
project manager in another Brazilian government ministry.


Our paper focuses on two particular partners: academia and
government. Some works discuss how academia can collaborate and transfer
knowledge to the industry in the management of software projects.
\cite{cho2011gap} evaluated the increasing use of agile techniques in software
development companies in Thailand. The authors suggested that universities
should create curricula that develop their undergraduate students' practical
skills required by industry (mainly agile practices) to promote growth in local
software businesses.  Our findings from the second questionnaire sent to former
undergraduates confirm the importance of including the development of practical
skills in undergraduate curricula. The SPB project was a successful case of
teaching and training IT professionals. The respondents reported that FLOSS and
Agile methodologies skills developed during the project made it easier for them
to get a job and also perform well their work activities.

\cite{sandberg2017iacollaboration} report the use of Scrum in
an industry-academia research consortium (involving ten industry partners and
five universities in Sweden). Through a case study, they demonstrate that being
able to bring together the meaningful activities of the stakeholders is
essential to the success of collaborative research between industry and
academia.  Similarly to theirs, our work also shows how the adoption of agile
values supports the collaboration of institutions with different goals.
\cite{sandberg2017iacollaboration} found nine practical ways to help
industry-academia collaborations, including organizing activities in sprints,
monthly face-to-face stakeholder meetings, using Scrum routines, joint
development planning, and delivery of intermediate versions. Their results have
a close connection with four practices reported in our paper that describe
Decision 2 (D2) made in the SPB project.


\cite{mergel2016} conducted a study including interviews with
managers of the central digital transformation team from the U.S.  government.
The author investigated the existing initiatives for agile management
implementation and proposed a research framework to guide future investigations
regarding collaborative and agile innovation management approaches in
government. This framework includes research questions such as: ``How can
government incentivize open sharing of source code instead of reinventing the
wheel with every request for proposals, signed contract or grant?''.  The SPB
Portal itself, the target of our study, is a case study that answers this
question: the portal's primary purpose is providing an environment to share
software developed for or by any of the many Brazilian government agencies,
reducing the costs of software and services acquisition. The development of the
platform also addressed this point: the platform is a system-of-systems that
has adapted and integrated five existing FLOSS projects, whose development was
supported by the management decisions and practices listed in this study.


Discussions on how to introduce new management methods into an
organization are present in the
literature~\citep{qumer2008,misra2009,mishra2011}. However, \cite{nuottila2015}
highlights that only a few works investigated agile adoption in public
organizations. Based on a case study from the Finnish government, they
conducted research ``to identify and categorize the challenges that may hinder
efficient adoption and use of agile methods in public IT projects''. The study
presents seven challenges: Documentation (C1); Education, experience, and
commitment (C2); Stakeholder communication and involvement(C3); Roles in the
agile set-up (C4); Location of the agile teams (C5); Legislation (C6);
Complexity of software architecture and system integration (C7). The decisions
discussed in our paper are directly related to the challenges C1, C2, C3 and
C7.  Our Decision 1 (D1) provided benefits such as organic documentation
(B1.5), communicating with transparency and efficiency (B1.1), and more
interactions between developers and public servants (B1.3). The benefit B1.5 is
related to challenge C1 while B1.1 and B1.3 concern C3. Benefits from Decision
2 (D2) such as reducing communication misunderstanding (B2.1), better meeting
expectations of both sides (B2.2), synchronizing the execution pace of
activities (B2.5), strengthening trust in the relationship with the government
(B2.7), and sharing a common understanding of the process from one side to the
other (B2.8) also are related to challenge C3. Our Decision 2 (D2), in specific
the practice P2.4 (build a Continuous Delivery pipeline with stages involving
both sides), also provided benefit B2.6 (Shared responsibility using Continuous
Delivery), associated with challenge C7.  Lastly, Decision 3 (D3) brought
benefits such as improving management and technical knowledge (B3.2), promoting
team self-training with knowledge transfer (B3.3), providing opportunities for
more advanced undergraduates to evaluate management issues and participate in
business decisions (B3.4), and self-organizing and gaining autonomy in the
management of their tasks (B3.5) which correlates to challenge C2.

Finally, \citet{nerur2015challenges} and \citet{impactOfOrganizationalCulture}
(discussed in Section \ref{sec:background}) argue that the adoption of agile
development techniques does indeed produce changes in an organization's
culture. Conversely, our study does not discuss significant changes in the
involved Brazilian government agency, since the strategy reported here had the
premise that both organizations (academia and government) should collaborate
with minimum impact on their internal macro-management processes and culture.
As seen in this section, such form of collaboration yields positive outcomes
mostly similar to other endeavors.

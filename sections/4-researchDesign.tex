\section{Research Design}
\label{sec:ResearchDesign}

A government-academia collaboration project demands strong inter-institutional
management, which involves financial issues, requirements and priorities
definitions, and possible divergences in the methodologies and development
techniques to be employed. In most cases, this kind of software development
partnership sets up work teams with different experiences, knowledge, maturity,
and work performances. These managerial and organizational complexities
resemble the peculiarities found in FLOSS ecosystems, which also often involve
multiple institutions with different interests and backgrounds.

With this in mind, FLOSS and agile practices may adequately handle differences
in the pace between the institutions involved, improving the cooperation of
distinct teams and benefiting the software being developed. From the FLOSS
ecosystems, we can extract useful practices such as open communication, project
modularity, development of a community of users, and fast response to reported
issues~\citep{capiluppi, warsta}. Besides, individuals and interactions,
working software, customer collaboration, and responsiveness to
change~\citep{beck} are agile values also suitable in this context.

\subsection{Research Questions}

As mentioned before, we are interested in handling the different development
processes at play in collaborative projects, primarily in government-academia
joint ventures. To guide this discussion, we focus on two research questions:

\myparagraph{RQ1.}\textit{How to introduce FLOSS and agile best practices into
government-academia collaboration projects?}

We begin by examining 30 months of a government-academia collaboration project
to identify managerial and organizational changes. This partnership enabled the
introduction of agile methods and FLOSS practices such as collaborative
environment development, use of a version control tool and discussion list,
continuous delivery, and self-organization. Our study aims to reveal ways to
bring such practices into the development process without sacrificing or
causing significant impacts on the internal procedures of the institutions
involved.

We focus our analysis on issues related to organizational differences and
diversity of project members regarding maturity and experience in collaborative
development. The harmony between teams sought not only to approximate the
mindset and work culture but also to circumscribe the interactions between
different roles and responsibilities.

\myparagraph{RQ2.}\textit{What FLOSS and agile practices favor effective team
management in government-academia collaborative projects?}

We continue by pinpointing how the introduction of FLOSS and agile methods has
improved interactions between the institutions as well as the internal
development teams. For this, we gathered opinions through interviews and
questionnaires from three different roles of the project: senior developers,
interns, and government staff. In addition to these responses, we also analyze
data documented in the project management and development platform to capture
the main benefits of the changes in the collaboration management and
development model.

\subsection{Research Method}

Due to the exploratory and explanatory character of RQ1 and RQ2, we used the case
study as the research method to answer these questions. This method is a good fit
when asking ``how'' or ``why'' questions and when we can directly
observe the events under study and interview persons involved in the activities
(two sources of evidence relevant for case studies)~\citep{yin2009}. Nonetheless,
we have also analyzed repository data to ratify our results.

To conduct the data analysis, we adopted
\textit{coding}~\citep{charmaz2008grounded}, the core strategy of Grounded
Theory~\citep{glaser1999gt,corbin2014slr}. It is a research approach widely-used
in software
engineering~\citep{waterman2015upfront,stol2016grounded,santos2016jobrotation,hoda2017transitions}.
We describe in detail our data analysis strategy in
Section~\ref{sec:data-analysis}.

\begin{figure}[ht]
  \centering
    \includegraphics[width=\linewidth]{figures/workflow2.pdf}
  \caption{Investigation workflow}
  \label{fig:methodology}
\end{figure}

Figure \ref{fig:methodology} summarizes our investigation workflow, which had as
its starting point previous works about the SPB
project~\citep{meirelles2017spb, siqueira2018, wen2018gacolab}. The rectangles
identify our research approaches, and the four following subsections describe
how we used each one. The light blue ellipses represent our findings
through the methods used. We explain and discuss them in
Section~\ref{sec:results-discussion}.


\subsubsection{Case study}
\label{sec:case}

A case study is the exhaustive study of a single case with the purpose of
enlightening a broader class of cases~\citep{gerring2006}.
According to \cite{yin2009}, the essence of a case study is to shed light on a
decision or a set of decisions, trying to understand the reasons and ways for
their implementation and the lessons learned. With this in mind, we chose the SPB
Portal as a case study because, besides being a high-quality example of a
well-balanced government-academia managerial process, it offered a valuable
opportunity to explore the benefits and challenges of using
FLOSS~\citep{kon2011,deKoenigsberg2008,fagerholm2013,fagerholm2014} and
Agile~\citep{steghofer2016,harzl2017,muller2018} practices for Software Engineering due
to the diversity of actors involved and the relationships between government,
academia, and industry.

On the academic side, the project team was composed of 42 undergraduate
interns, two college professors, six developers with significant experience in
the FLOSS tools integrated into the portal, and two UX specialists from the IT
market. On the government side, the project was managed by two requirement
analysts and a board of directors composed of one department
director and one department coordinator.
%
While government staff was more used to traditional project management
approaches, the academic members believed that the use of agile methods was more
appropriate for the development of the proposed platform. As a consequence,
conflicts between the internal management processes and differences in pace and
goals of each institution started to compromise the platform development.
%
Moreover, during the project, the government board of directors changed and,
with it, the vision of the project, affecting the previously approved project
requirements.
%
Under these circumstances, the professors who coordinated the project made
decisions in a non-systematic way, incrementally adopting best practices
derived from FLOSS and agile values to improve the project management process.
%
Finally, during the six initial months, the project received only a fraction of the planned
funding for the period, pushing the coordinators to restrict work to formatting
the process to be used by the project, in particular through training
undergraduate interns on the relevant tools. Much of the project activity from
this period focused on requirements definition, tools selection, and other
architectural decisions, as well as team building, including handling the bureaucracy to hire
the interns and the professionals from FLOSS communities and from the IT market.

The SPB portal project underwent two phases regarding the traceability of project
management activities. The first one, between January 2014 and March 2015, is
non-traceable, since only the universities managed the development activities.
%
In April 2015, the project started using the SPB portal itself to manage the
development process, inaugurating the second phase of the project. From then
on, much of the management and communication activities were recorded and
published in online channels and tools. During this period, the development
leaders consolidated several FLOSS practices and agile values employed in the
development process.

This study brings forth observations of researchers who worked directly in the
project coordination, researchers who participated in the internal processes,
and researchers who did not participate in the project. The first two groups
mapped the practices adopted during the project and analyzed and
explained the experience previously
reported~\citep{meirelles2017spb,siqueira2018} by some of the authors of this
paper. The others contributed their expertise in FLOSS and agile methods
studies and helped the inside researchers define macro-decisions and interpret,
with a more neutral perspective, the benefits and effects of these best
practices on the project management context as preliminary presented
by~\cite{wen2018gacolab}.

\subsubsection{Survey}
\label{sec:survey}

For us to obtain a good overview of how participants perceived multiple aspects
of the project, we needed to survey them. However, as mentioned, the SPB
project had a wide range of stakeholders. Accordingly, to guide
the creation of the necessary questionnaires and interviews as well as the analysis
of the collected data, we divided the project team into three groups: undergraduate interns,
IT professionals (senior developers and designers), and MPOG analysts. Besides
each member's point of view (obtained from interviews and questionnaires described here), we
also had other sources of evidence, such as data from the code repository and
from the activity management tool (as presented in Section
\ref{sec:descricao-repo}).

We designed an online
questionnaire\footnote{\url{https://gitlab.com/ccsl-usp/spb-jss-2018/raw/master/data-collection/questionnaires/interns-questions-1.md}}
addressed to the undergraduate interns
and another\footnote{\url{https://gitlab.com/ccsl-usp/spb-jss-2018/raw/master/data-collection/questionnaires/senior-developers-questions.md}}
addressed to the IT professionals, as well as
semi-structured interviews~\citep{edwards2013qualitative}
with the two MPOG analysts who directly interacted with the development team
and the project development process.
%
To design the questionnaires and interview questions, we followed principles of
survey research~\citep{pfleeger2001-survey-p1}: searching the relevant
literature, constructing the instrument~\citep{kitchenham2002-survey-p3}, and evaluating the
instrument~\citep{kitchenham2002-survey-p4}. In
particular, we used the lessons learned reported in our previous
work~\citep{meirelles2017spb} to reduce the overhead of survey
construction~\citep{kitchenham2002-survey-p3}. Moreover, the authors of this study
that participated directly in different roles of the project brought an improved
comprehension of each interaction and aided in contacting the participants. Thereby, we
conducted several brainstorming conversations with three ex-interns and two
senior developers. This step, also recommended by the classical Ground Theory
approach~\citep{glaser1999gt}, provided a better understanding of the relevance
of the project for the involved participants. All these principles
and actions ultimately helped us shape the questionnaires and
interview questions used as survey instruments.

\mysubsubsub{Questionnaires}

The questionnaire for the undergraduate interns dealt
with topics related to (1) project
organization, (2) development process, (3) communication and relationship with
members, (4) acquired knowledge, and (5) experience with FLOSS projects. To
cover these topics, we elaborated a total of 51 questions (45 multiple-choice
questions and six open questions).
%
The questionnaire for the IT professionals was comprised of
29 multiple-choice questions and 10 open questions, covering the following
topics: (1) project organization; (2) project and stakeholders development
processes; (3) communication and relationship with undergraduate interns; (4)
the product developed; and (5) experience with FLOSS projects.

For both questionnaires, we designed multiple-choice questions for a more
direct evaluation of the evolution of the methods used for activity management,
of the organization of the project, and of the quality of the
communication among all parties involved. We used open-ended questions to
gather more nuanced perceptions.

The questionnaires were in Portuguese (the native language of the
participants). The questionnaire guide translated into English is available in our
repository\footnote{\url{https://gitlab.com/ccsl-usp/spb-jss-2018/tree/master/data-collection/questionnaires}}.
We used the Google Forms service\footnote{\url{docs.google.com/forms}} to
implement them and obtain response
charts\footnote{\url{https://gitlab.com/ccsl-usp/spb-jss-2018/tree/master/data-collection/questionnaires/responses}}.
%
Before submitting the questionnaires to the participants,
we performed a test-completion of both to estimate response
time.  We also conducted a trial with Computer Science Graduate Students at the
University of São Paulo and IT professionals with similar profiles to the
respondents. Thereby, we could confirm the readability and correct
interpretation of the questions as well as fix spelling mistakes and replace
ambiguous terms found by testers. We also redefined some response formats: For
instance, we opted for the Likert scale over a numerical range in opinion
closed-questions, because the Likert scale makes the respondent more
comfortable to decide on a suitable answer.

We sent the questionnaires to 42 interns and 8 IT professionals. All interns
worked as developers and received scholarships. We got a total of 37 (88\%)
intern responses, while all 8 IT professionals responded. On average,
interns were 22 years old and professionals were 30 years old, wherein 3 of 37 (8\%) and
1 of 8 (13\%) respectively were women. 16 of 37 (about 43\%) of the interns had the SPB project as
their first contact with FLOSS. On average, the IT professionals had 11 years
of experience, worked in at least 5 different companies, and participated in 4
to 80 distinct projects.  Finally, 7 of 8 (86\%) of them had some background with FLOSS
before the SPB project.

\mysubsubsub{Interviews}

We conducted semi-structured interviews~\citep{edwards2013qualitative}
with the two MPOG analysts who directly interacted with the development team
and the project development process. Semi-structured interviews are more
spontaneous than structured interviews: They allow much more space for
interviewees to answer on their terms. In a typical semi-structured interview,
the interviewer follows an interview guide but is flexible to deviate from the
predefined topical trajectory when he or she finds it appropriate and
productive. He or she can probe answers, ensue a dialogue, and discuss a
proposed theme better.

We initially defined an interview protocol with 28 questions in Portuguese, and
shared it with respondents. The protocol translated into English is
available in our
repository\footnote{\url{https://gitlab.com/ccsl-usp/spb-jss-2018/raw/master/data-collection/interviews/mpog-interview.md}}.
The questions guide had four parts: (1) Professional profile;(2) Organization,
communication, and development methodologies; (3) Satisfaction with the
developed platform; and (4) Lessons learned. Before conducting the interviews,
we reviewed the protocol with one project coordinator and four development team
coaches who interacted directly with the MPOG analysts.  Being a
semi-structured interview, the interviewer conducted the interview placing each
question according to the pace and meaning of each response. The
interviewer also added additional questions during data collection to clarify or
detail some interesting things mentioned by the interviewee.

Each interviewee was invited by email to indicate the date, time, and location
most convenient for the interview.  The interviews were conducted separately
with each analyst via video-conference, using the Google Hangouts platform.
Each meeting took an average of two hours and was carried out outside the
working environment. Respondents agreed to have their voice and image recorded
for this study. The interviewer was a member of the project team and had
already met the interviewees at an earlier time. Both respondents were over 30
years old and had more than seven years working in the government. The analysts
said that the SPB project represented their first experience of
government-academia collaboration.

After two years of the project conclusion, we sent a second
questionnaire\footnote{\url{https://gitlab.com/ccsl-usp/spb-jss-2018/raw/master/data-collection/questionnaires/interns-questions-2.md}}
to the same group of undergraduate interns to ratify the results obtained in
the previous survey and evaluate the impacts of the project experience on the
professional life of these interns.
%
This was a short-questionnaire with two closed questions, aimed at
drawing a brief updated profile of the participants, and three open questions
about the professional activities performed by them and the unfolding of the
knowledge acquired in the project on these activities. 
%
We made the questionnaire available to receive responses for seven
days, reaching 28 respondents of the 42 interns.

\subsubsection{Repository Data Collection}
\label{sec:descricao-repo}

Although most of the development team already had experience with agile
methodologies and the use of tools such as Redmine\footnote{\url{https://redmine.org}} and
GitLab in the development process, these resources were not part of the
government's administrative culture. Consequently, in the first phase of the
project (as explained in Section~\ref{sec:case}), these differences in managerial culture, coupled
with the trust relationship still under construction between the two
institutions, led to non-integrated management processes in the same
project. The communication between government and academia was, generally,
through private channels, such as professional e-mails, personal meetings, and
telephone calls. Therefore, the quantitative data found for this period are not
conclusive or have little expressiveness, and we do not examine them.

In contrast with this, the second phase of the project, when management
activities shifted to the SPB Portal tools, was characterized by the automatic
collection of a significant amount of meaningful data. This data source enabled
us to reach a wealth of managerial information available on the project platform. We
manually analyzed these data from the central project repository considering
all the issues and commits from this second phase.

The project code was structured in the GitLab instance of the SPB platform. The
development team defined a central repository, which concentrated on the
customizations of the software. They also had local mirrors for the
repositories of each integrated tool, where the team developed general
improvements of each project for later submission of contributions. Within this
structure, the dialogue between government and academia always took place
around the central repository. The data collected from the repositories, which
we discuss in Section~\ref{decisions:repo-discussion}, was divided into two
categories:

\begin{description}

\item \textit{Development data}

  \begin{itemize}
  \item The number of commits per project made in the GitLab instance of the platform (Central, Noosfero, Colab, etc.).
  \item The number of different authors of commits registered in the repositories.
  \item The number of issues per project (Central, Noosfero, Colab, etc.).
  \item The number of different authors of issues with their respective identifier and author name.
  \end{itemize}

\item \textit{Interaction data}

  \begin{itemize}
  \item List of issues per project, ordered by ascending date.
  \item Date of the first issue opened by some MPOG staff (author).
  \item The number of comments on each issue, with names and emails of the participating authors.
  \end{itemize}

\end{description}


\subsubsection{Data Analysis}
\label{sec:data-analysis}

Software engineering researchers have adopted Grounded Theory in recent years and regularly
use it to analyze interviews and descriptive
field-notes~\citep{sbaraini2011grounded}. 
%
With a \textit{coding} strategy from the Grounded Theory
approach~\citep{glaser1999gt,corbin2014slr}, we broke down and labeled the data
into smaller components~\citep{sbaraini2011grounded}. Based on this strategy,
our analysis comprehends five steps:


\myparagraph{(i) Data review:} We organized and consolidated the data from the
questionnaires and interviews (described in Section~\ref{sec:survey}). During
the process, we reviewed the records again, transcribing the full interviews.
These transcripts preserve the interviewed speech, maintaining details such as
colloquial expressions.\\

		\textit{``A very positive meeting was about software
		qualification, talking to the team to get a level of detail of
		the feature they could understand, and all the questions were
		answered.''}\\

		After that, we read the responses of the questionnaires
		open-questions. We reviewed the transcriptions and the form
		responses to fix typos, fill in small gaps in sentence
		construction, and map non-familiar terms. An example of non-familiar terms, according
		to the above transcription quote, is:\\
                
		\textit{\myparagraph{Software Qualification} $\rightarrow$ The
		feature developed to evaluate software available on the SPB
		platform}.\\

		We also unified the meaning of some terms found:\\

		\textit{\myparagraph{Version Control:} GitLab, version management, versioning}.

		\textit{\myparagraph{Continuous Delivery:} 
		setup management, Devops, automation, integration
		delivery, continuous delivery, automated tests}.
        

\myparagraph{(ii) Practices and Benefits -- Open coding:} We identified
expressions (codes) that could relate to the appropriate actions or perceptions
described in the descriptive field-notes and the interviews.  These initial
codes are labels that represent the first set of mapped practices and benefits
to be consolidated throughout all the analysis. This step is exemplified as
follows -- examining a piece of transcriptions related to the
``government-academia interaction'':

\begin{enumerate}[label=(\alph*)]
	\item ``..robust interaction \textit{via the Internet}''


	\item ``..almost in \textit{real-time}.''

	\item ``..use of
		\textit{mailing lists}, [..]
		\textit{personal email}, [..]
		\textit{Hangouts chat}.''
	
	\item ``..a lot of \textit{discussion in the project itself} (with GitLab subsystem).''
	
	\item ``..\textit{video-conferences} to answer questions with the whole team distributed between 3--4 cities.''
	
	\item ``I had \textit{no communication problem}.''

	\item ``..the \textit{frequency of dialogues increased and evolved when the
		communication has migrated to the GitLab}.''
	
	\item ``..the interaction itself was \textit{open and accessible}.''
	
	\item ``..the interaction was mostly \textit{by email, GitLab, hangout, and even phone calls}.''

\end{enumerate}

Given the list above, we could extract the practices related to
``government-academia interaction'' from items (a), (c), (d), (e), (i), while the
benefits derived from these practices are described by (b), (f), (g), (h).

We also examined the responses to the questionnaire open-questions related to
``government-academia interaction'' to map practices and derived benefits:

\begin{enumerate}[label=(\alph*)]
	
	\item ``\textit{Joint planning} and
		\textit{seasonable meetings} were essential for
		\textit{understanding MPOG's needs}.''

	\item ``\textit{Interaction via SPB tools} helped
		\textit{validate the system} as a development platform.''

\end{enumerate}


\myparagraph{(iii) Decisions -- from codes to categories:} Based on the initial
list of practices and benefits (the codes), we conducted a comparative analysis
to identify similar expressions in the data, categorizing the labels to group
codes of the same semantics. We can exemplify this process by taking the above
example of open coding of transcription. Practices outlined in items (a), (c),
(d), (i), and consequently the related benefits (f), (g), (h), fell under the
same category related to ``Government and Academia interact through the system
under development''. By completing this process, all practices and benefits
reached some category (corresponding to one of the decisions discussed in Section
\ref{sec:results}).

\myparagraph{(iv) Decisions refinement:} In the previous step, we determined
three categories. In this step, we did not change the number of mapped
decisions. However, we renamed them to better describe each one, such as
\textit{\textbf{(category)} ``Government and Academia interact through the system under
development''} to
\textit{\textbf{(decision)} ``Use of the system under development to
develop the system itself''}.

We also consolidated the group of practices and benefits related to each
decision to support the found categories (summarized in
Table~\ref{practices-table}, Section \ref{sec:results-discussion}).

\myparagraph{(v) From decisions to long-term benefits:} Having identified the refined
decisions, we surveyed the ex-interns (former undergraduate students), who
joined the development team, in a second round. We examined how 
the identified decisions and practices reflected in their professional
life after the project. Thereby, we obtained enough data to highlight the
``Benefits for Teaching Software Engineering'' as discussed in
Section~\ref{sec:teachingSE}.

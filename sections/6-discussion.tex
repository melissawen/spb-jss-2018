\subsection{Adopted practices and consequences}
\label{sec:discussion}

Providing a high-quality product and overcoming the differences in pace and
interest of stakeholders were possible with the non-systematic employment of
practices obtained from agile methodologies and FLOSS ecosystems. These
practices emerged from the mentioned strategic decisions that guided the overall
development of the project.

%\input{sections/decision-practise-benefit-table.tex}

Our previous work~\citep{wen2018gacolab} revealed a set of nine managerial
practices that resulted in 14 benefits evidenced by replies to questionnaires
and analysis of repository data. Table~\ref{practices-table}, from the previous
section, presents an update of the summary of macro-decisions, practices, and
benefits found in that work, as well as 5 new identified benefits.
Based on the data set above, we analyzed all questionnaire's responses and
performed a critical review of the effects of these practices on both the
management of collaboration and the professional training of undergraduate
students. After showcasing the major decisions and their benefits in the
previous section, we now proceed to discuss the individual practices and how
each one brought the benefits previously highlighted.

\subsubsection{Day-to-day practices and benefits achieved}

\myparagraph{P1.1}\space \textit{The features and tools of the platform under development support the project management and communication activities.} As soon as a first beta version of the new portal became available, the new SPB project, including both the development process and the project management activities, migrated to it.
Discussions and technical decisions originally occurred in a dispersed way through difficult-to-monitor communication media such as telephone, e-mail, and face-to-face contact. The government and academia teams then started to keep their dialogues registered in the repository issues, wiki pages, and list of project discussions available on the platform.

\begin{shiftdesc}
  \item[B1.1] \textit{Communicating with transparency and efficiency.} The use of a mailing list and the specification of requirements through issues on GitLab made the discussions and decisions available publicly and in real time.
  \item[B1.2] \textit{Easy monitoring of activities.} Each code addition was versioned and directly related to the activity that originated it. Coordinators and developers could allocate contingency activities faster and monitor solutions adopted as well as all the code generated for it.
  \item [B1.3] \textit{More interactions between developers and public servants.} Using online resources, made available in CMS and community format, broke physical and mental barriers of dialogue between the government team and the university team.
  \item [B1.4] \textit{Confidence in the developed code.} Daily interactions with the software brought to the surface all bugs and accelerated the software improvement process.
  \item [B1.5] \textit{Organic documentation.} Decisions, meeting reports, and changes were automatically recorded in the issue tracker, wiki, and mailing list.
\end{shiftdesc}

\myparagraph{P2.1}\space\textit{Government staff, academic coordinators, senior developers, and team coaches meet biweekly at the university lab for sprint planning and review.} Initially, only the academic development team participated in the development follow-up routines, and leaders of academia moved to the government headquarters to meet the government team for determining the features to be delivered in the next four months. There was a gap in time and space between institutions that was softened by defining a more dynamic schedule, where the government team also attended meetings held at the university and participated more frequently in achieving the requirements.

\begin{shiftdesc}
  \item [B2.1] \textit{Reducing communication misunderstanding.} In-person meetings generate empathy among team members and solve problems of interpretation due to errors in written expression and understanding thanks to speech tone.
  \item [B2.4] \textit{Overcoming the government bias regarding low productivity of collaborative projects with academia.} Government staff realized the intensity of the agile development and shared the difficulties encountered during the construction of a feature.
\end{shiftdesc}

\myparagraph{P2.2}\space\textit{Direct contact, with no bureaucratic barriers, between government staff and the development team on the platform technical discussions.} Stakeholders from both the government and the development team were encouraged to discuss issues and features directly among each other instead of going through the chain of command.

\begin{shiftdesc}
  \item [B2.2] \textit{Better meeting of expectations on both sides.} Managing development through GitLab provide a greater approximation of all stakeholders to the development process, technical decisions, and deliveries of functionality.
\end{shiftdesc}

\myparagraph{P2.3}\space\textit{Involve government board of directors only in strategic planning of the project.} Due to the directors' schedule, they could not keep up with the unfolding of technical decisions. To better explore the few meetings with the presence of the directors, discussions with them were restricted to strategic business issues, leaving the planning of the development process to the more frequent reunions with only the MPOG analysts.

\begin{shiftdesc}
  \item [B2.3] \textit{Improvement of the decision-making process.} Technical details no longer interfered with strategic decisions, and political issues also did not affect technical choices. These restrictions generated less wear and tear among the teams and enriched the discussions of technical subjects among those better acquainted with them.
\end{shiftdesc}

\myparagraph{P2.4}\space\textit{Build a Continuous Delivery pipeline with stages involving both sides.} The development and management processes became automated, providing guidance, clarity of responsibilities, and rhythm to all teams involved.

\begin{shiftdesc}
  \item [B2.5] \textit{Synchronizing the execution pace of activities.} The CD pipeline performance depended on the synchronization between the academia and government teams, as each party had to be prepared to take action as soon as the other concluded a given task. The use of an explicit CD pipeline helped identify critical points of delay, and increased productivity.
  \item [B2.6] \textit{Shared responsibility using Continuous Delivery.} According to the conventional MPOG process, the development team could not track what happened to the code after its delivery, since their employees were the only ones responsible for deployment. The implementation of CD made the development team feel equally responsible for what was getting into production and take ownership of the project \citep{shahin2016}.
  \item [B2.7] \textit{Strengthening trust in the relationship with the government.} With CD, intermediate and candidate versions became available to be validated in the biweekly meetings, allowing the government staff to perform small validations over time. Constant monitoring of the development work brought greater assurance to the MPOG leaders and improved the interactions with the development team.
  \item [B2.8] \textit{Sharing a common understanding of the process.} The steady pace and faster deployment provided by the pipeline brought a better overall view of the project for all participants.
\end{shiftdesc}

\myparagraph{P3.1}\space\textit{Coordinators separate the development team into priority work areas considering the main demands of the project.} The project was divided into four work fronts, two to address general concerns of User eXperience and DevOps and two aimed at the development of the integrated applications that provided substantial functionalities to the platform: Noosfero and Colab.

\begin{shiftdesc}
  \item [B3.1] \textit{Conciliating the development processes of each institution, taking better technical decisions.} With this division, both the government and academia teams began to describe better the requirements for identifying the responsible work front and correctly reference feedback, bugs, and necessary adjustments. Each work front has become a core of knowledge, developing among them technical skills according to the work scope. This improved the argumentative and explanatory capacity of the development team to technical issues raised by the government team.
\end{shiftdesc}

\myparagraph{P3.2}\space\textit{IT market professionals with recognized experience on each front were hired to work in person or remotely.} With the division of the team, experienced professionals in the field of information architecture, design, front-end, infrastructure, as well as creators and maintainers of Noosfero, Colab, and other FLOSS projects joined to undergraduate interns in the different work fronts. Due to the high cost of moving this category of professionals, many worked remotely for the project.

\begin{shiftdesc}
  \item [B3.2] \textit{Improving the management and technical knowledge.} The contracted professionals brought the team their technical abilities and experiences with industry, large-scale project and collaborative development. In addition to developing in the team the ability to organize and interact remotely,  increasingly common in today's work environments.
  \item [B3.3] \textit{Promoting team self-training with knowledge transfer.} Through peer programming, code review, and consultations, senior developers have transferred their knowledge to interns. Undergraduate interns could organically absorb a high load of technical expertise during the performance of their activities which would not be possible in the classroom context.
\end{shiftdesc}

\myparagraph{P3.3}\space\textit{Identify among the interns the leadership roles: a coach for each front, and a meta-coach of the entire development team.} The academic coordinators invited the interns who had good maturity and knowledge of their work front to leadership and coaching roles. The one who acquired a good perception of the whole development process was then chosen to lead the dynamics between teams and the dialogues between the development team and the government team.

\begin{shiftdesc}
  \item [B3.4] \textit{Providing opportunities for more advanced undergraduates to evaluate management issues and participate in business decisions.} When the intern took on the role of coach, he represented the team in the overall planning of the development of requirements and at times participated in direct meetings with government staff and board directors. These interactions enabled him to develop leadership and management skills.
\end{shiftdesc}

\myparagraph{P3.4}\space\textit{Each team self-organizes, being guided by one intern-coach and at least one senior developer.} The intern-coach was responsible for planning and managing the activities and relied on senior developers' maturity and experience to schedule tasks, identify bottlenecks and overcome technical difficulties.

\begin{shiftdesc}
  \item [B3.5] \textit{Self-organizing and gaining autonomy in the management of their tasks.} The closeness and openness in communication between the coach with the other interns summed with the maturity and market experience of the senior developers provided a balance in the development of the tasks of each work front and self-organization of the team according to the abilities of each one.
  \item [B3.6] \textit{Channeling professors' efforts to address high-level issues and to manage collaboration bureaucracies.} Self-manageable work fronts allowed the academic coordinators (university professors) to be more available for definition of strategic goals with the government and management of the financial and contractual project issues, without compromising their teaching activities at the university.
\end{shiftdesc}

The results presented in this paper corroborate the lessons learned in our previous work
on studying the SPB project case \citep{meirelles2017spb}. Evidence from the data
collected, responses to questionnaires, and interviews reinforce what has been
reported by the academic coordination of the project, adding the point of views
of government and other roles involved on the academic side. In short, the
government staff took time to understand how collaboration works and to realize
that the project should not assume a client-executor relationship, but rather
that both organizations were at the same hierarchical level in the work plan.

\subsubsection{Benefits for Teaching Software Engineering}
\label{sec:teachingSE}


After about two years of SPB project conclusion, in addition to contributions for
the management of future partnerships between government and academia, this
experience reverberated in the professional life of both senior developers and
government agents, as well as the interns -- former undergraduate students. The experience
of a real project during the academic training allowed these newly graduated
students to reach advanced positions in the universe of academia, private
initiative, entrepreneurship, and FLOSS. The ex-interns revealed in their
responses of the second round questionnaire that:

\begin{quote}
\textit{``The project helped me develop my masters and personal
	projects.''}, \textit{``It was the gateway to the job market and contributed
	significantly to my current job''}.
\end{quote}

{71\%} of these ex-interns stated that the use of agile methods in a large-scale
project with real clients was the primary professional contribution of the
project since such practices are part of their current workflow.

\begin{quote}
\textit{``Everything that I learned during the SPB project I could employ on my
	current job, from best development practices to the use of Colab.''},
	\textit{``The project experience was essential to start in the job
	market, especially the experience with agile methodologies and their
	procedures''}.
\end{quote}

{68\%} of them have reported that DevOps abilities and versioning using git are
knowledge from SPB project that improves their current work performance. For
{36\%}, understanding of the collaborative development and the cycle of
contributions to FLOSS projects are insights they apply in their professional
and academic activities.

\begin{quote}
\textit{``Nowadays I am in the development area, and the DevOps knowledge
	acquired during the project helps me identify and solve
	problems faster.''}, \textit{``The knowledge of real projects, deliveries,
	scopes, and deadlines was essential to my professional performance''}.
\end{quote}

In summary, because the SPB portal platform was not a ``toy project'', the
heterogeneity of its project actively encouraged these ex-interns to work on
their personal and professional maturity and to search for practical techniques
of versioning, infrastructure, and DevOps.

\begin{quote}
\textit{``The experience with a multidisciplinary team reflects directly on my
	current job.''}, \textit{``The project experience was essential to
	define what I want to work with during my life and gave me the framework not to
	start my professional activities without a sense of real software
	development.''}), \textit{``Now I can see the project as a substantial
	professional and personal growth experience. The integration between
	senior developers and undergraduate interns ripened the team in
	different moments, helping the team to uncover problems promptly, and
	search for reasonable alternatives or best ways to change. The
	adoption of agile practices also led us to a good dynamic of
	development''}.
\end{quote}


In addition to the delivered new version of the new SPB portal itself, this
study also reveals that the adopted practices have benefited the people
involved in the SPB rebuilding project. After we analyzed the
\textit{post-mortem} data of the SPB project, we noticed this project
extrapolated FLOSS and agile values by impacting the lives of the people
involved. Most of the former interns answered the second questionnaire: a
total of 29 respondents. Five of them are working in large companies or
Brazilian government agencies, three became entrepreneurs, and 11 are Computer
Science or Information System researchers, which means 19 out of 29 respondents
(65\%). Additionally, several of the participants still actively contribute to
large FLOSS communities, such as Debian, Fedora, Linux, Spark, Elixir, among
others. Senior developers from the SPB rebuilding project have recruited some
of the ex-interns they helped to train to become their co-workers. In summary,
the project benefited the training of several students significantly.

# Study about the experience of former students that participated in the SPB project

If you allow, we will use your answers anonymized in a research we are conducting about the SPB. To answer the questionnaire, you will take about 8 minutes.

## About my anonymized answers in this questionnaire*

* [ ] I allow the use of anonymized data that I provide in this questionnaire about the SPB

# Profile

Age*

Gender
  * Female
  * Male
  * I'd rather not say

What is you current occupation?
  * Undergraduate student
  * Graduate student
  * Entrepreneur
  * Freelancer
  * Developer/designer in a small company
  * Developer/designer in a medium/large company
  * Civil servant
  * Professor
  * Unemployed
  * Other

# During the project

In which levels did you play a part on the project?*
* [ ] Undergraduate student
* [ ] MsC student
* [ ] Full developer

In which teams did work on?*
* [ ] Colab
* [ ] Noosfero
* [ ] DevOps
* [ ] Design

What was the team you most contributed to?*
* Colab
* Noosfero
* DevOps
* Design

When did you star working at the SPB?*
1. First semester of 2016
2. Second semester of 2015
3. First semester of 2015
4. Second semester od 2014
5. First semester of 2014

When did you finish your activities at the SPB?*
1. First semester of 2016
2. Second semester of 2015
3. First semester of 2015
4. Second semester od 2014
5. First semester of 2014

Were you a team leader?*
* Yes
* No

Did you want to be a team leader?*
* Yes
* No
* Indifferent
* I don't know/I don't want to opine

# About your experience during the project

In your opinion, which of the activities below brought more value during the Sprint?*
* [ ] Planning
* [ ] Design/Discuss the architecture
* [ ] Programming
* [ ] Writing tests/Testing
* [ ] Code review
* [ ] Sprint review

Did you see value in giving points to user stories?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Did you feel relevant changes between retrospectives?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

During your participation in the project, did you feel that the team leader's (coach) presence was essential to the Spring functioning?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Was it working in teams important to you during the project?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Was it working with seniors important to you during the project?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Did pair programming with other colleagues contribute to your learning?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Did pair programming with seniors contribute to your learning?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Was it interacting with a real client (MPOG employees) important to you during the project?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

Was it releasing new SPB versions motivating to you during the project?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

In your opinion, was it the participation of the clients (MPOG employees) in Sprint plannings and completions important to the project's development?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

What did you think of writing requirements along with the client (MPOG employees)?*
* Very important
* Mildly important
* A little important
* Not important at all
* I don't know/I don't want to opine

Did you have the will, at some point of the project, to participate in meetings with the MPOG?*
* Yes
* No
* I don't know/I don't want to opine

In a few words, how would you describe the interaction between the development team and the MPOG employees in the project?

Do you have any other comments about your experiences in the project?

# About the code

At the end of your participation in the project, did you understand the general architecture of the SPB platform?*
* 1 (Did not understand)
* 2
* 3
* 4
* 5 (Very good understanding)

At the end of your participation in the project, did you understand the architecture of the project (Colab, Noosfero, DevOps, Design) you worked on?*
* 1 (Did not understand)
* 2
* 3
* 4
* 5 (Very good understanding)

Regarding the team you most contributed to, did you feel comfortable to fix bugs?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* I don't know/I don't want to opine

Regarding the team you most contributed to, did you feel comfortable to add new features?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* I don't know/I don't want to opine

Regarding the team you most contributed to, did you feel comfortable to review other people's merge requests or code?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never reviewed a merge request
* I don't know/I don't want to opine

Did you find it necessary for the project the presence of a separate DevOps team?*
* Very important
* Important
* A little important
* Unnecessary
* I don't know/I don't want to opine

Do you have any other coments about your technical involvement with the project?

# Communication

Did you feel comfortable to talk in stand-up meetings?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never talked in stand-up meetings
* I don't know/I don't want to opine

Did you feel comfortable in pair programming with colleagues?*
* Very comfortable
* Mildly comfortable
* A litle comfortable
* Not comfortable at all
* I don't know/I don't want to opine

Did you feel comfortable in pair programming with seniors?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never pair programmed with seniors
* I don't know/I don't want to opine

Did you feel comfortable to talk in a videoconference with the presence of seniors?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never talked in videoconferences
* I don't know/I don't want to opine

Did you feel comfortable to ask seniors questions remotely?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never asked seniors questions remotely
* I don't know/I don't want to opine

Did you feel comfortable to ask seniors questions face-to-face?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never asked seniors questions face-to-face
* I don't know/I don't want to opine

Did you feel comfortable to talk directly with the client (MPOG employees)?*
* Extremely comfortable
* Very comfortable
* Comfortable
* A litle comfortable
* Not comfortable at all
* Never talked with MPOG employees
* I don't know/I don't want to opine

Do you have any other comments about the communication in the project?

# About free software

Was it the first time you contributed to free software?*
* Yes
* No
* I don't know

Did you contribute to the same projects outside SPB work hours?*
* Yes
* No
* I don't remember/I don't want to opine

Do you still contribute to the projects you were involved to during the SPB?*
* Yes
* No
* I don't remember/I don't want to opine

About your interaction with the projects community, which of the below communication channels you used more often?*
* [ ] IRC
* [ ] Mailing list
* [ ] Merge request
* [ ] Issues
* [ ] Did not interact with the community
* [ ] I don't know/I don't want to opine

Did you have help from free software community's of the projects you contributed to during the project?*
* A lot
* Moderate
* A little
* None
* Did not ask for help
* I don't remember/I don't want to opine

Did you contribute to other free software projects outside SPB work hours?*
* Yes
* No
* I don't remember/I don't want to opine

What was your main motivation to contribute to free software?*
* Never contributed
* Money
* Technical learning
* Social interaction
* Work opportunities
* Appealing project
* I don't know/I don't want to opine

Do you believe that contributing to a free software project to be important professionaly?*
* Yes
* No
* Indifferent
* I don't know/I don't want to opine

After the project, do you still contribute to any free software? If positive, which ones?*

In your opinion, in which ways contributing to free software can help other people and/or the society?

Do you have any other comments about your involvement with free software?
* 
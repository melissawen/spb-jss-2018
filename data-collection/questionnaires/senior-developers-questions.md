# Study about the experience in the SPB project

If you allow, we will use your answers anonymized in a research we are conducting about the SPB.

## About my anonymized answers in this questionnaire*

* [ ] I allow the use of anonymized data that I provide in this questionnaire about the SPB

# Interviewee's profile

How old are you?*

What is your gender?*
  * Female
  * Male
  * I'd rather not say

What is you current occupation?*
  * [ ] Graduate student
  * [ ] Entrepreneur
  * [ ] Freelancer developer/designer
  * [ ] Developer/designer in a small company
  * [ ] Developer/designer in a medium/large company
  * [ ] Civil servant
  * [ ] Professor
  * [ ] Unemployed
  * [ ] Other

How many years of professional experience do you have?*

In how many companies have you worked on?*

In how many projects (in general) have you worked on?*

# Pariticipation in the project

In which teams did work on?*
* [ ] Colab
* [ ] Noosfero
* [ ] DevOps
* [ ] Design

What was the team you most contributed to?*
* Colab
* Noosfero
* DevOps
* Design

How many months did you work in the project for?*

In which of the below activities did you participate during the Sprint?*
* [ ] Sprint Planning
* [ ] Design the SPB architecture
* [ ] Programming
* [ ] Writing tests/Testing
* [ ] Code review
* [ ] Sprint review

# Experience with the students in the project

From your perspective, which of the activities below (with your involvement) contributed the most to the students evolution in the project?*
* [ ] To participate in the Sprint planning
* [ ] To participate in SPB's architectural decisions
* [ ] To pair program with a senior
* [ ] To write tests
* [ ] To participate in joint review activities (code, screen prototypes, etc)
* [ ] To participate in the Sprint review

Did you feel that the presence of student team leaders (coaches) was essential for your interaction with the team?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

Did the changes in coaches (student leaders) impact your iteraction (or productivity) in the project?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

In your opinion, did you feel that you conributed to the learning process of the students that interacted with you?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

In the development process point of view, did you feel that your orientations, when passed to one or a set of students, were understood by the majority of the team? For example, did you feel safe in changing the focus to another task or did you feel insecure for having to repeat the same information a number of times?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

How would you describe your interaction with the students during the project? Do you have any thoughts or facts that are worth being pointed out about it?

# Experience with the development process

Regarding the development process adopted in the project, do you think you collaborated in the evolution of that process by adding practices based on your previous experiences? For example, DevOps culture, code review, requirements validation by screen prototypes, etc.*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

Regarding the project's development process in comparison with your experience in previous projects: were there more similarities or differences? For example, DevOps culture, code review, requirements validation by screen prototypes, etc.*
* Very similar
* Mildly similar
* A little similar
* Not similar at all
* I don't know/I don't want to opine

In general, did you agree with the project's development process?  For example, DevOps culture, code review, requirements validation by screen prototypes, etc.*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

About the interaction with other seniors of the project: were there any knowledge exchange?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

Was it releasing new SPB versions motivating to you during the project?*
* A lot
* Moderate
* A little
* None
* I don't know/I don't want to opine

How would you describe the development process during the project? If possible, report how was the work relationship with UnB/LAPPIS students. What was positive? What were the difficulties? Was there learning for you too?

# Experience in the project with the client (MPOG)

In which of the below meetings did you attend to?*
* [ ] Strategic planning (with MPOG's director, coordinator and analysts)
* [ ] Release planning (with MPOG's analysts)
* [ ] Sprint planning (with MPOG's analysts)
* [ ] Sprint review (with MPOG's analysts)
* [ ] None

Did you understand the management process of MPOG's projects?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

Did the management process of MPOG's projects influence your interaction (or productivity) in the project?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

Do you think you helped in the relation between the MPOG's management processes and the project's development (UnB)?*
* A lot
* Moderate
* A little
* Not at all
* I don't know/I don't want to opine

During the project, how do you evaluate the client's (MPOG analysts) participation in the project's planning, development and validation?*
* Very active
* Mildly active
* A little active
* I don't know/I don't want to opine
 
Regarding the client's (MPOG analysts) requirements/demands/interests during the project, did you help the MPOG analysts to understand and/or to express more clearly what they were requesting?*
* Many times
* Sometimes
* A few times
* Never
* I don't know/I don't want to opine

In a few words, how would you describe the interaction between the development team (UnB) and the clients (MPOG analysts) in the project?*

Could you report one bad and one good experience of interacting with the client (MPOG)? In the case of the negative one, what did you do to mitigate the problem?*

# About the artifacts created (code, screen prototypes, server infrastructure, documents, etc)*

How would you describe the contributions you and the teams you worked on as a senior made?

# Communication

How did you communicate with the UnB team?*
* [ ] IRC
* [ ] Mailing list
* [ ] Issue tracker
* [ ] Videoconferences
* [ ] Phone
* [ ] Personally

How did you communicate with the MPOG team?*
* [ ] IRC
* [ ] Mailing list
* [ ] Issue tracker
* [ ] Videoconferences
* [ ] Phone
* [ ] Personally

How would you describe the communication process during the project? Communication with students, MPOG analysts and project coordination (professors).*

Do you have any other comments about te communication in the project?

# About free software

Before the project, what was your experience in contributing to free software projects?*
* A lot of experience
* Moderate experience
* A little experience
* No experience
* I don't know/I don't want to opine

If you are a free software developer, during the SPB, was there a increase or decrease in the rhythm and quality of your contributions in the projects you already colaborated on?*

Do you still contribute to the projects you were involved during the SPB? If positive, which ones? Did this rhythm increase or decrease in comparison with the period you worked on the project?*

Do you have any other comments regarding your involvement in free software projects? In case you have never contributed before the SPB, could you describe how was this experience to you?*

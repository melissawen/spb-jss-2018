# Research study with former SPB project members - additional questions

Age

Age when received the undergradute degree

Which professional or academic activities are you currently involved? If you are working on free software projects, cite them as well.*
* Report all professional/academic activities you are involved. Cite the name of the institution related to the activity, if you feel comfortable.

What is your perception about how much the experiences acquired in the SPB project contributed to your current professional or academic activity?*
* In case your are working on free software projects, consider them in your answer as well.

Which practices or knowledge used during the SPB project do you apply in your current professional or academic ativities?*
* In case your are working on free software projects, consider them in your answer as well.
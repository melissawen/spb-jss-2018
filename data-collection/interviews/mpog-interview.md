### Resaerch project - Interview

### Context

Between 2014 and 2016 a platform was developed to the Brazilian Public Software Portal. The project's goal was to evolve an already existing portal, adding features from previously mapped requirements by the Ministry of Planning. In this project, the platform became a composition of integrated different free software systems, aiming to fulfill all requirements and to make it easier to maintain and to update the portal as a whole. The project had an analysts team from the Ministry of Planning, UnB professors (that were also project coordinators) and a development team composed of UnB interns and IT professionals. The government team had its own work dynamics and software development process, directed by governmental laws and guidelines. With the professor's assistance and supervision, more recent methodologies, studied in the academia, were applied to enhance team productivity, product deliverables quality and interpersonal relationships between the teams. This partnership between teams with different development processes went through conflicts and adaptations. During the 3 years of project, many ways to conciliate the processes, to improve the communication and to make the platform's development clearer and more transparent to those who accompanied, managed and developed the new platform were thought, applied and evaluated.

### The focus:
* To explore possible ways to conciliate teams with different managerial and development processes in one project

### What is my position in the context of the study:
* Working for a government's temporary software development project

### What are the previous experiences or beliefs that may lead to partiality (bias):
* We believe that agile development is better than waterfall development
* The development team is bigger than the analysts team
* Different work experiences: private companies vs government
* We are not involved in internal government bureaucracies regarding the project
* We don't have work stability and the project has a deadline (it also can be cancelled hurriedly in case there are flaws)
* In spite of everyone working for the project's success, we didn't feel really united. Did we sometimes feel like rivals?
* The development team was mainly composed by students, without any work experience
* We didn't participate on the government's decisions, strategies and reports

### Political influences:
* The analysts may not want to expose internal problems or show disbelief or question their work context
* Interference of political or partisan beliefs of the interviewers regarding the project's context and client decisions

### General research questions we wanto to answer
1. How can we harmonize teams with different software development processes in the same project?
 * Client satisfaction and developers (team) motivation levels. Is it possible to maintain a team motivated and clients satisfied with both comfortable in communicating with each other even though speaking different "languages"?
2. How can we stablish a threshold between the managerial questions of the involved organisations (Ministry and UnB) and the development team management?
 * We are arguing that the project's management process need not to, necessarily, be the same and influence directly on the software development methodology.
 * To filter communication conflicts between teams with different mentalities and methodologies
 * "Give in to win"

----

## Interview - Questions

### Professional and personal profile

1. Introduction: Could you make a brief personal and professional introduction? Name, age, education, professional work experience, time at the Ministry, current sector and job, sector and job at the time of the project.
2. Same as the SPB, what other software development software in partnership between the Ministry and others institutions have you worked on?
3. What are the similarities and differences you can identify among those projects and the SPB?
4. Was the SPB your first software development experience in partnership with a university? What are the advantages and disadvantages you can identify in this kind of partnership?

### About the SPB project

#### Organization

5. How was the project structured and organized initially? How did it work the communication and follow-up? Who were the actors and interlocutors?
6. What were the changes the happened during the project in comparison with its initial formatting?
 7. In your opinion, were these changes positive or negative? Why?
 8. Did you encounter any difficulties in adapting to those changes? Which ones?
9. Did any change occur during the project regarding the platform's initial requirements? If positive, would you be able to point some of them? Why was the change necessary?
 10. How would you describe the UnB development team's adapation regarding the requirement changes?
11. How were the release planning meetings carried out? Where was it, who participated, what were the goals, what was discussed, what were the difficulties...
12. How was the follow-up on the release development made?
13. Were the project's planning and follow-up similar to MPOG's?
 14. Did the differences impact your performance at work? If positive, in what ways?

#### Communication

15. How did you interact with the UnB team? Who were the people/roles in the UnB team that you interacted more?
16. Did you interact with the project's senior developers? When those interactions happened, what was the communication channel used and what was the motive to start the communication?
17. Did you interact with the project's students? When those interactions happened, what was the communication channel used and what was the motive to start the communication?
18. How can you describe the communication between the MPOG and the coordinator of the UnB team?
19. How can you describe the communication between the MPOG and the development team?
20. In the development's team organization, specific teams were created for each softaware composing the platform. Did you feel up to date with those changes? Did you recognize the people during each communication/interaction? Do you believe this knowledge was important and influenced in your activities while keeping track of the project?
21. During the project, did you feel the communication got better or worse between the MPOG and the development team? To which factors do you credit it to?
 22. Can you remember any negative or hard to handle interaction between the teams?
 23. Can you remember any positive or clarifying interaction?

#### Delivery methodology/development process

24. Generally speaking, how is the software development process at MPOG? How and when are the software requirements defined, how is the follow-up and the validation and homologation made?
25. How was the conception, the development process and maintenance of the old SPB platform?
26. What were the differences between that internal process and the one conducted by the UnB team?
 27. What were the difficulties/conflicts between those development processes that most impacted the work and responsibilities at MPOG? What was made to ease this impact?
 28. Did the incremental process bring you discomfort or difficulties? Can you give examples?
29. In your opinion, was it possible to conciliate the UnB and MPOG development processes? What was necessary to adapt to conciliate what was demanded internally by the MPOG and the information collected during the development?
30. During the project, the MPOG started to go more often to the university and to participate on-site in the planning and delivery meetings. In your opinion, what were the advantages and disadvantages in that interaction? Did it impact in any way the feedback quality and feature validation?
31. During the homologation of a delivery, how would you describe the team's capacity in fixing or changing a feature given the MPOG's feedback?
32. In general, what was the MPOG's satisfaction regarding what had been planned and what was delivered at the end of each release? Why?

#### SPB's platform
33. How satisfied were the MPOG and users regarding the previous platform? Did it fulfill the requirements previously planned? What were the main complaints and new requests?
34. How would you describe the platform developed to the SPB?
35. Do you think that the platform's complexity is suitable with its features?
36. Did you have trouble in understanding the tools or the platform as a whole? Can you give examples?
37. Do you feel confident regarding the delivered code?
38. Do you feel confident regarding the platform's features?

#### Last thoughts
39. If you had to evaluate a future partnership between the MPOG and the university, on which points would you pay more attention? Why? What advises would you give the MPOG team that would conduct the project?
